package annotation;

import java.awt.*;

public class DeprecatedExample {

    public static void main(String[] args) {
        oldFun();
        newFun();
    }

    @Deprecated
    public static void oldFun() {
        System.out.println("old function called");
    }

    public static void newFun() {
        System.out.println("new fun called");
    }

}

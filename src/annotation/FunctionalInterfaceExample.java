package annotation;

public class FunctionalInterfaceExample {

}

//functional interface must have only one abstract method
@FunctionalInterface
interface AccountInterface {
//    void getAccount();
    void getName();
}

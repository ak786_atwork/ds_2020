package annotation;

//avoid user mistake spelling   -- will get error at compile time
public class OverrideAnnotationExample {
    public static void main(String[] args) {
        Parent parent = new Child();
        parent.show();
    }
}

class Parent {
    public void show() {
        System.out.println("i am super");
    }
}

class Child extends Parent {
    @Override
    public void show() {
        System.out.println("i am child");
    }

}
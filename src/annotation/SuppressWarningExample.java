package annotation;

import test.A;
import test.test1.B;

import java.util.ArrayList;

public class SuppressWarningExample {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.getCustomerDetails();
    }
}

class Bank {
    @SuppressWarnings("unchecked")
    public ArrayList getCustomerDetails() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("a");
        arrayList.add("b");
        arrayList.add("r");
        return arrayList;
    }
}
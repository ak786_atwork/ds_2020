package annotation.meta;

import java.lang.annotation.*;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface CourseAnnotation {
    String cid() default "c11";
    String cname() default "java";
    int ccost() default 10000;
}

package annotation.meta;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class Student {

    String sid;
    String sname;
    String saddr;

    public Student(String sid, String sname, String saddr) {
        this.sid = sid;
        this.sname = sname;
        this.saddr = saddr;
    }

    @CourseAnnotation(cid ="c90",cname = "java",ccost = 30000)
    public void getDetails() {
        System.out.println("id = "+sid+" name = "+sname+" address = "+saddr);
    }
}

class Driver {
    public static void main(String[] args) throws Exception {
        Student student = new Student("124","anil","bhiwani");
        student.getDetails();
        System.out.println("-------------------------");

        //3 approach
        // 1.class.forName
        //2. getclass()
        //3. .class file name

        /*
        //for class level annotation
        Class c = student.getClass();
        Annotation annotation = c.getAnnotation(CourseAnnotation.class);
        CourseAnnotation courseAnnotation = (CourseAnnotation)annotation;

        System.out.println("cid = "+courseAnnotation.cid()+" cname = "+courseAnnotation.cname()+" cost = "+courseAnnotation.ccost());*/

        Class c = student.getClass();
        Method method = c.getMethod("getDetails");

        Annotation annotation = method.getAnnotation(CourseAnnotation.class);
        CourseAnnotation courseAnnotation = (CourseAnnotation)annotation;

        System.out.println("cid = "+courseAnnotation.cid()+" cname = "+courseAnnotation.cname()+" cost = "+courseAnnotation.ccost());


    }
}

package cci;

public class BasicBitOperation {
    public static final int MAX_VALUE = Integer.MAX_VALUE;
    public static void main(String[] args) {
        printInBits(-99999,32);
        printInBits1(99999,32);
        System.out.println(Integer.toBinaryString(99999));
    }

    public static void printInBits(int number, int numberOfBits) {
        if (number < 0) {
            number = getMaxValue(numberOfBits) + number;
        }
        int mod = 0;
        StringBuilder sb = new StringBuilder();
        while (numberOfBits-- > 0) {
            mod = number%2;
            sb.append(mod);
            number /= 2;
        }
        System.out.println(sb.reverse());
    }

    private static int getMaxValue(int numberOfBits) {
//        return numberOfBits == 32 ? Integer.MAX_VALUE : (int) Math.pow(2,numberOfBits);
//        System.out.println((int) Math.pow(2,numberOfBits) + " "+ Integer.MAX_VALUE);
        return  (int) Math.pow(2,numberOfBits);
    }

    public static void printInBits1(int number, int numberOfBits) {
        StringBuilder sb = new StringBuilder();
        int maskBit = 0;
        while (numberOfBits-- > 0) {
            maskBit = (number & 1);
            number = number >>> 1;
            sb.append(maskBit);
        }

        System.out.println(sb.reverse());
    }

}

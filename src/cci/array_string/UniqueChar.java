package cci.array_string;

/**
 * All approaches
 * 1. brute force O(n2) - comparing each char with another
 * 2. hashtable O(n) O(n) -
 * 3. bit vector - implemented below - scoped char-set
 * 4. if no space is allowed and unscoped char-set is there  - go for sorting
 *
 *
* */


public class UniqueChar {
    public static void main(String[] args) {
        String s = "abcsea";
        String[] tests = {"aaaa", "abcda", "asdfghjklqwertyuiopzxcvbnm"};

        for (String word : tests)
            System.out.println(isUnique(word));
    }

    /**
     * using bit vector approach
     * <p>
     * assuming string contains a-z
     */
    private static boolean isUnique(String s) {
        //we have only a-z ie 26 allowed chars, if it exceeds that means duplicates are there
        if (s.length() > 26)
            return false;

        int checker = 0;
        int val = 0;

        //this also can be max of 26 ie o(c) - where c is number of allowed chars
        for (int i = 0; i < s.length(); i++) {
            val = s.charAt(i) - 'a';
            if ((checker & (1 << val)) > 0) {
                return false;
            }
            checker |= 1 << val;
        }

        return true;
    }

}

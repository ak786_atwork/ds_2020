package design_pattern;


abstract class TV {
    Remote remote;

    public TV(Remote remote) {
        this.remote = remote;
    }

    abstract void on();
    abstract void off();
}

interface Remote {
    void on();
    void off();
}


class Sony extends TV {

    Remote remoteType;

    public Sony(Remote remote) {
        super(remote);
        remoteType = remote;
    }

    @Override
    void on() {
        System.out.println("Sony TV on");
        remoteType.on();
    }

    @Override
    void off() {
        System.out.println("Sony tv is off");
        remoteType.off();
    }
}


class Phillips extends TV {

    Remote remoteType;

    public Phillips(Remote remote) {
        super(remote);
        remoteType = remote;
    }

    @Override
    void on() {
        System.out.println("Sony TV on");
        remoteType.on();
    }

    @Override
    void off() {
        System.out.println("Sony tv is off");
        remoteType.off();
    }
}

class OldRemote implements Remote {

    @Override
    public void on() {
        System.out.println("old remote turn off command");
    }

    @Override
    public void off() {
        System.out.println("old remote turn on command");
    }
}


class NewRemote implements Remote {

    @Override
    public void on() {
        System.out.println("new remote turn off command");
    }

    @Override
    public void off() {
        System.out.println("new remote turn on command");
    }
}

public class BridgePatternExample {

    public static void main(String[] args) {
        TV sonyTV = new Sony(new OldRemote());
        sonyTV.on();
        sonyTV.off();
        System.out.println();

        TV sonyTV1 = new Sony(new NewRemote());
        sonyTV1.on();
        sonyTV1.off();
        System.out.println();

        TV pTV = new Sony(new OldRemote());
        pTV.on();
        pTV.off();
        System.out.println();

        TV pTV1 = new Sony(new NewRemote());
        pTV1.on();
        pTV1.off();
        System.out.println();





    }

}

package design_pattern;

import java.util.ArrayList;
import java.util.List;

abstract class Account {
    public abstract float getBalance();
}


class SavingAccount extends Account{

    private String accountNumber;
    private float accountBalance;

    public SavingAccount(String accountNumber, float accountBalance) {
        super();
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }

    @Override
    public float getBalance() {
        return accountBalance;
    }
}

class DepositAccount extends Account{

    private String accountNumber;
    private float accountBalance;

    public DepositAccount(String accountNumber, float accountBalance) {
        super();
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }

    @Override
    public float getBalance() {
        return accountBalance;
    }
}


class CompositeAccount extends Account{

    private List<Account> accounts = new ArrayList<>();
    private float totalBalance;

    public void addAccount(Account account){
        accounts.add(account);
    }

    public void removeAccount(Account account) {
        accounts.remove(account);
    }

    @Override
    public float getBalance() {
        totalBalance = 0;
        for (Account account : accounts)
            totalBalance += account.getBalance();
        return totalBalance;
    }
}






public class CompositePatternExample {

    public static void main(String[] args) {
        CompositeAccount compositeAccount = new CompositeAccount();
        compositeAccount.addAccount(new SavingAccount("DA1",123));
        compositeAccount.addAccount(new DepositAccount("DA2",123));

        System.out.println(compositeAccount.getBalance());
    }

}

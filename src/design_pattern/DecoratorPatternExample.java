package design_pattern;

interface Dress{
    void assemble();
}

class  BasicDress implements Dress {

    @Override
    public void assemble() {
        System.out.println("basic dress features");
    }
}

class DressDecorator implements Dress {
    private Dress dress;

    public DressDecorator(Dress dress) {
        this.dress = dress;
    }

    @Override
    public void assemble() {
        this.dress.assemble();
    }
}

class CasualDress extends DressDecorator {

    public CasualDress(Dress dress) {
        super(dress);
    }

    @Override
    public void assemble() {
        super.assemble();
        System.out.println("Adding casual dress features");
    }
}

class SportyDress extends DressDecorator {

    public SportyDress(Dress dress) {
        super(dress);
    }

    @Override
    public void assemble() {
        super.assemble();
        System.out.println("Adding Sporty dress features");
    }
}

class FancyDress extends DressDecorator {

    public FancyDress(Dress dress) {
        super(dress);
    }

    @Override
    public void assemble() {
        super.assemble();
        System.out.println("Adding fancy dress features");
    }
}





public class DecoratorPatternExample {

    public static void main(String[] args) {
        Dress sportyDress = new SportyDress(new BasicDress());
        sportyDress.assemble();

        Dress fancyDress = new FancyDress(new BasicDress());
        fancyDress.assemble();

        Dress casualDress = new CasualDress(new BasicDress());
        casualDress.assemble();

        System.out.println();
        Dress sportyFancyDress = new SportyDress(new FancyDress(new BasicDress()));
        sportyFancyDress.assemble();

        System.out.println();
        Dress sportyfancycasualDress = new SportyDress(new FancyDress(new CasualDress(new BasicDress())));
        sportyfancycasualDress.assemble();


    }

}

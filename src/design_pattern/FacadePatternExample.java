package design_pattern;

import java.sql.Driver;

class FireFox {
    public static Driver getFireFoxDriver() {
        return null;
    }

    public static void generateHTMLReport(String test, Driver driver) {
        System.out.println("generating html report using firefox driver");
    }

    public static void generateJUnitReport(String test, Driver driver) {
        System.out.println("generating junit report using firefox driver");
    }
}

class Chrome {
    public static Driver getFireFoxDriver() {
        return null;
    }

    public static void generateHTMLReport(String test, Driver driver) {
        System.out.println("generating html report using Chrome driver");
    }

    public static void generateJUnitReport(String test, Driver driver) {
        System.out.println("generating junit report using chrome driver");
    }
}

class WebExplorerHelperFacade{

    public static void generateReports(String explorer, String report, String test) {
        Driver driver = null;
        switch (explorer) {
            case "firefox" :
                driver = FireFox.getFireFoxDriver();
                switch (report) {
                    case "html" :
                        FireFox.generateHTMLReport(test, driver);
                        break;

                    case "junit" :
                        FireFox.generateJUnitReport(test, driver);
                        break;
                }
                break;

            case "chrome" :
                driver = Chrome.getFireFoxDriver();
                switch (report) {
                    case "html" :
                        Chrome.generateHTMLReport(test, driver);
                        break;

                    case "junit" :
                        Chrome.generateJUnitReport(test, driver);
                        break;
                }
                break;
        }
    }
}

public class FacadePatternExample {

    public static void main(String[] args) {
        String test = "checkElement";
        WebExplorerHelperFacade.generateReports("chrome", "html",test);
        WebExplorerHelperFacade.generateReports("chrome", "junit",test);
        WebExplorerHelperFacade.generateReports("firefox", "junit",test);
        WebExplorerHelperFacade.generateReports("firefox", "html",test);
    }
}

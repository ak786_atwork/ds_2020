package design_pattern;

import test.test1.B;

abstract class Vehicle {

    public abstract int getWheel();

    public String toString() {
        return "Wheel " + this.getWheel();
    }
}

class Bike extends Vehicle {

    int wheel;

    public Bike(int wheel) {
        this.wheel = wheel;
    }

    @Override
    public int getWheel() {
        return this.wheel;
    }
}

class Car extends Vehicle {
    int wheel;

    public Car(int wheel) {
        this.wheel = wheel;
    }

    @Override
    public int getWheel() {
        return this.wheel;
    }
}

//naming convention superclass name +  factory
class VehicleFactory {
    public static Vehicle getInstance(String type, int wheel) {
        if (type == "car") {
            return new Car(wheel);
        } else if (type.equals("bike"))
            return new Bike(wheel);
        else
            return null;
    }
}


public class FactoryPatternDemo {
    public static void main(String[] args) {
        Vehicle car = VehicleFactory.getInstance("car", 4);
        Vehicle bike = VehicleFactory.getInstance("bike", 2);

        System.out.println("bike = " + bike + " car = " + car);
    }
}



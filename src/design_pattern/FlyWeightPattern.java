package design_pattern;

import java.util.HashMap;
import java.util.Random;

interface Employee {
    void assignSkill(String skill);
    void task();
}

class Developer implements Employee {

    private final String JOB;
    private String skill;

    public Developer() {
        JOB = "FIX THE ISSUE";
    }

    @Override
    public void assignSkill(String skill) {
        this.skill = skill;
    }

    @Override
    public void task() {
        System.out.println("Developer with skill "+skill+" job "+JOB);
    }
}

class Tester implements Employee {

    private final String JOB;
    private String skill;

    public Tester() {
        JOB = "TEST THE ISSUE";
    }

    @Override
    public void assignSkill(String skill) {
        this.skill = skill;
    }

    @Override
    public void task() {
        System.out.println("Tester with skill "+skill+" job "+JOB);
    }
}

class EmployeeFactory {
    private static HashMap<String, Employee> m = new HashMap<>();

    public static Employee getEmployee(String type) {
        Employee p = null;
        if (m.containsKey(type)) {
            p = m.get(type);
        } else {
            switch (type) {
                case "developer" :
                    System.out.println("developer created");
                    p = new Developer();
                    break;

                case "tester" :
                    System.out.println("tester created");
                    p = new Tester();
                    break;

                default:
                    System.out.println("no such employee");
            }
            m.put(type,p);
        }
        return p;
    }
}



public class FlyWeightPattern {
    private static String employeeType[] = {"developer", "tester"};
    private static String skills[] = {"c", "java","c++"};

    public static void main(String[] args) {
        for(int i=0;i<10;i++) {
            Employee e = EmployeeFactory.getEmployee(getRandomEmployee());
            e.assignSkill(getRandomSkill());
            e.task();
        }
    }

    private static String getRandomSkill() {
        return skills[new Random().nextInt(skills.length)];
    }

    private static String getRandomEmployee() {
        return employeeType[new Random().nextInt(employeeType.length)];

    }
}

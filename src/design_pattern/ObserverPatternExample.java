package design_pattern;


import java.util.ArrayList;
import java.util.List;

interface Subject {
    public void registerObserver(Observer observer);
    public void unRegisterObserver(Observer observer);
    public void notifyObservers();
}

interface Observer {
    void update(String location);
}

class DeliveryData implements Subject {

    private List<Observer> observers;
    private String location;

    public DeliveryData(){
        observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unRegisterObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers)
            observer.update(location);
    }

    public void locationChanged() {
        this.location = getLocation();
        notifyObservers();
    }

    private String getLocation() {
        return "place";
    }

    public String toString() {
        return "reached at "+location;
    }
}


class Seller implements Observer {

    private String location;

    @Override
    public void update(String location) {
        this.location = location;
        showLocation();
    }

    private void showLocation() {
        System.out.println("Notification at seller : current location : "+location);
    }
}

class Customer implements Observer {

    private String location;

    @Override
    public void update(String location) {
        this.location = location;
        showLocation();
    }

    private void showLocation() {
        System.out.println("Notification at Customer : current location : "+location);
    }
}




public class ObserverPatternExample {

    public static void main(String[] args) {
        Seller seller = new Seller();
        Customer customer =  new Customer();
        DeliveryData deliveryData = new DeliveryData();

        deliveryData.registerObserver(seller);
        deliveryData.registerObserver(customer);

        deliveryData.locationChanged();
        deliveryData.unRegisterObserver(seller);
        deliveryData.locationChanged();
        deliveryData.unRegisterObserver(customer);
        deliveryData.locationChanged();



    }

}

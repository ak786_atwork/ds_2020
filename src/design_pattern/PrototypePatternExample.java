package design_pattern;

import java.util.ArrayList;
import java.util.List;

class Vehicle2 implements Cloneable {
    private List<String> vehicleList;

    public Vehicle2() {
        this.vehicleList = new ArrayList<>();
    }

    public Vehicle2(List<String> vehicleList) {
        this.vehicleList = vehicleList;
    }

    public void insertData() {
        vehicleList.add("honda");
        vehicleList.add("baleno");
        vehicleList.add("i20");
    }

    public List<String> getVehicleList() {
        return vehicleList;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        List<String> tempList = new ArrayList<>();
        for (String s : vehicleList)
            tempList.add(s);

        return new Vehicle2(tempList);
    }
}


public class PrototypePatternExample {

    public static void main(String[] args) throws CloneNotSupportedException {
        Vehicle2 vehicle = new Vehicle2();
        vehicle.insertData();

        Vehicle2 vehicle2 = (Vehicle2) vehicle.clone();
        List<String> list = vehicle2.getVehicleList();
        list.add("i10");

        System.out.println(vehicle.getVehicleList());
        System.out.println(vehicle2.getVehicleList());
        System.out.println(list);

        vehicle2.getVehicleList().remove("i10");
        System.out.println(list);

    }

}

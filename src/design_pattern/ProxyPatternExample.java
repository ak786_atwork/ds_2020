package design_pattern;

interface DatabaseExecutor {
    void executeQuery(String query) throws Exception;
}

class DatabaseExecutorImpl implements DatabaseExecutor {

    @Override
    public void executeQuery(String query) throws Exception {
        System.out.println("Going to execute query");
    }
}

class DatabaseExecutorProxy implements DatabaseExecutor {

    boolean ifAdmin;
    DatabaseExecutorImpl databaseExecutor;

    public DatabaseExecutorProxy(String name, String password) {
        if (name.equals("admin") && password.equals("admin")) {
            ifAdmin = true;
            databaseExecutor = new DatabaseExecutorImpl();
        }
    }

    @Override
    public void executeQuery(String query) throws Exception {
        if (ifAdmin) {
            databaseExecutor.executeQuery(query);
        } else {
            if (query.equals("delete")) {
                throw new Exception("DELETE not allowed for non admin users");
            } else {
                databaseExecutor.executeQuery(query);
            }
        }
    }
}

public class ProxyPatternExample {

    public static void main(String[] args) {
        DatabaseExecutor notAdmin = new DatabaseExecutorProxy("ad","fsd");
        try {
            notAdmin.executeQuery("delete");
        } catch (Exception e) {
            e.printStackTrace();
        }

        DatabaseExecutor admin = new DatabaseExecutorProxy("admin", "admin");
        try {
            admin.executeQuery("delete");
            System.out.println("admin no crash");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

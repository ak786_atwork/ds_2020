package design_pattern;

import org.omg.CORBA.ObjectHelper;

public class Singleton {

    public static void main(String[] args) {
        printName(SingletonLazy.getINSTANCE());
        printName(SingletonLazy.getINSTANCE());
    }

    private static void printName(Object instance) {
        System.out.println(instance);
    }

}

class SingletonEager {
    private static SingletonEager INSTANCE = new SingletonEager();

    private SingletonEager() {
    }

    public static SingletonEager getINSTANCE() {
        return INSTANCE;
    }
}

class SingletonLazy {
    private static SingletonLazy INSTANCE;

    private SingletonLazy() {
    }

    public static SingletonLazy getINSTANCE() {
        if (INSTANCE == null)
            INSTANCE = new SingletonLazy();
        return INSTANCE;
    }
}

//Method synchronization
class SynchronizedSingletonLazy {
    private static SynchronizedSingletonLazy INSTANCE;

    private SynchronizedSingletonLazy() {
    }

    public static synchronized SynchronizedSingletonLazy getINSTANCE() {
        if (INSTANCE == null)
            INSTANCE = new SynchronizedSingletonLazy();
        return INSTANCE;
    }
}


//block synchronization
class BlockSynchronizedSingletonLazy {
    private static BlockSynchronizedSingletonLazy INSTANCE;

    private BlockSynchronizedSingletonLazy() {
    }

    public static synchronized BlockSynchronizedSingletonLazy getINSTANCE() {
        if (INSTANCE == null) {
            synchronized (BlockSynchronizedSingletonLazy.class) {
                if (INSTANCE == null)
                    INSTANCE = new BlockSynchronizedSingletonLazy();
            }
        }
        return INSTANCE;
    }
}

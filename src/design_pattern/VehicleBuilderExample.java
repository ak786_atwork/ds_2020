package design_pattern;

class Vehicle1 {
    //required parameter
    private String engine;
    private int wheel;

    //optional parameter
    private int airbags;

    public String getEngine() {
        return this.engine;
    }

    public int getWheel() {
        return this.wheel;
    }

    public int getAirbags() {
        return this.airbags;
    }

    public String toString() {
        return " engine = "+this.engine +" wheel = "+this.wheel+ " airbags = "+this.airbags;
    }

    private Vehicle1(VehicleBuilder builder) {
        this.wheel = builder.wheel;
        this.airbags = builder.airbags;
        this.engine = builder.engine;
    }


    public static class VehicleBuilder {
        private String engine;
        private int wheel;
        private int airbags;

        public VehicleBuilder(String engine, int wheel) {
            this.engine = engine;
            this.wheel = wheel;
        }

        public VehicleBuilder setAirbags(int airbags) {
            this.airbags = airbags;
            return this;
        }

        public Vehicle1 build() {
            return new Vehicle1(this);
        }
    }
}




public class VehicleBuilderExample {
    public static void main(String[] args) {
        Vehicle1 car = new Vehicle1.VehicleBuilder("1200cc", 4).setAirbags(4).build();
        Vehicle1 bike = new Vehicle1.VehicleBuilder("200cc", 2).build();

        System.out.println("bike = "+bike + "\n car = "+car);
    }
}

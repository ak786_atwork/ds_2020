package generic;

import test.A;

import java.util.ArrayList;

public class TestGeneric {
    public static void main(String[] args) {
//        ArrayList list = new ArrayList();
//        list.add("name");
//        list.add(1);

        // java.lang.ClassCastException: java.lang.Integer cannot be cast to java.lang.String
//        System.out.println(((String)list.get(1)));

        Gen<String> stringGen = new Gen<>("anil");
        stringGen.show();
        stringGen.getObj();

        BoundedGen<Integer> boundedGen = new BoundedGen<>(3,6);
//        BoundedGen<String> boundedGen = new BoundedGen<>(3,6);   incompatible bounds
        boundedGen.performOperations();

        GenMethods genMethods = new GenMethods();

        genMethods.m1(new ArrayList<String>());

    }
}

//generic class example
class Gen<T> {
    T obj;

    public Gen(T obj) {
        this.obj = obj;
    }

    public void show() {
        System.out.println("object type = "+obj.getClass().toString());
    }

    public T getObj() {
        return obj;
    }
}

class BoundedGen<T extends Number> {
    T a, b;

    public BoundedGen(T a, T b) {
        this.a = a;
        this.b = b;
    }

    public void performOperations() {

        System.out.println("operations = "+(a.intValue() + b.intValue()));
    }
}

class GenMethods{

    public static void main(String[] args) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        m1(stringArrayList);    //acceptable
        m2(stringArrayList);
//        m3(stringArrayList);    //not acceptable
//        m4(stringArrayList);    //not acceptable

        ArrayList<Integer> integerArrayList = new ArrayList<>();
//        m1(integerArrayList);   //not
        m2(integerArrayList);
        m3(integerArrayList);
//        m4(integerArrayList);  //not

        ArrayList<? extends Number> numberArraylist = new ArrayList<Double>();
        ArrayList<? extends Runnable> runnableList = new ArrayList<Thread>();
        ArrayList<? extends Runnable> myThreadList = new ArrayList<MyThead>();

        ArrayList<? super Runnable> superRunnableList = new ArrayList<Object>();  //super class of implementation classes, here Thread's superclass is Object.


    }


    public static void m1(ArrayList<String > arrayList) {
        //only String type we can add
        arrayList.add("anil");
        arrayList.add("abc");
        System.out.println(arrayList.toString());
    }
    public static void m2(ArrayList<?> arrayList) {
        //can be called by passing any type of arraylist, used for read only operations
        arrayList.add(null);
//        arrayList.add("abc");       //can't be applied
        System.out.println(arrayList.toString());
    }

    public static void m3(ArrayList<? extends Number> arrayList) {
        /* if number is a class then we can call this method by passing Number type or its child classes
            if   Number is an interface then it can be called by passing Number type or its implementation classes
        * */
        // used for read only operations
        arrayList.add(null);
//        arrayList.add(new Integer(3));   //can't be added
        double sum = 0;
        for (Number n: arrayList){
            sum += n.doubleValue();
        }

        System.out.println(arrayList.toString());
    }

    public static void m4(ArrayList<? super X> arrayList) {
        /* if X is a class then we can call this method by passing Number type or its super  classes
            if   X is an interface then it can be called by passing X type or its super classes of  implementation classes
            we can add X type surely and child type
        * */
        // used for read only operations
        arrayList.add(null);
        arrayList.add(new ZChild());

        System.out.println(arrayList.toString());
    }
}

interface X { }
class Y {

    //<T> just to tell that this is a generic method
    public <T> T m1(T obj) {
        return obj;
    }
}
class XChild extends Y implements X {}
class ZChild extends XChild {}

class MyThead extends Thread {}

class GenNonGenCommunication {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("abc");
//        list.add(10);       //cant add
        System.out.println(list);
        m1(list);
        System.out.println(list);
//        list.add(8);


        ArrayList ngList = new ArrayList();
        ngList.add("a");
        ngList.add(10);
        System.out.println(ngList);
        ngList.add("again");
        System.out.println(ngList);

        System.out.println("---------------");
        check();
    }

    public static void m1(ArrayList list) {
        list.add("anil");
        list.add(10);
        list.add(true);
    }

    public static void m2(ArrayList<String> list) {
        list.add("abc");
//        list.add(10);  //cant add
    }


    //to check generic is runtime concept or not
    public static void check() {
        ArrayList list = new ArrayList<String>();
        list.add("abc");
        list.add(19);
        list.add(true);

        System.out.println(list);

        //no compile time or runtime error, this means that generic is not applicable at runtime
    }

   /* name clash compile time error : generic concept available at compile time only
   public static void checkMethod(ArrayList<Integer> list) {}
    public static void checkMethod(ArrayList<String> list) {}*/
}
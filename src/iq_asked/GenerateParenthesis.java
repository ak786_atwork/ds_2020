package iq_asked;

import java.util.*;

//asked in paytm money
//submitted on leetcode
public class GenerateParenthesis {

    public static void main(String[] args) {
        System.out.println(generateParenthesis(3));
        System.out.println((new Solution()).generateParenthesis(3));
    }

    public static List<String> generateParenthesis(int n) {
        HashSet<String> resultSet = new HashSet<>();
        resultSet.add("()");

        for (int i=1;i<n;i++) {
            HashSet<String> currentSet = new HashSet<>();
            //add () to all elements at all possible places and add to set

            for (String s : resultSet) {
                addParenthesisToAllPossiblePlaces(currentSet, s);
            }
            resultSet = currentSet;
        }

        return new ArrayList<>(resultSet);
    }

    private static void printSet(HashSet<String> resultSet) {
        System.out.println(resultSet.toString());
    }

    private static void addParenthesisToAllPossiblePlaces(Set<String> currentSet, String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0;i<s.length();i++) {
            sb.insert(i,"()");
            currentSet.add(sb.toString());
            sb.delete(i,i+2);
        }
    }
}

//from leetcode
class Solution {
    public List generateParenthesis(int n) {
        List list=new LinkedList<>();
        StringBuilder sb=new StringBuilder();
        generate(list,sb,0,0,n);
        return list;
    }

    private void generate(List list,StringBuilder sb,int open,int close,int n)
    {
        if(sb.length()==n*2)
        {
            list.add(sb.toString());
            return;
        }
        if(open<n){
            generate(list,sb.append("("),open+1,close,n);
            sb.deleteCharAt(sb.length()-1);
        }

        if(close<open){
            generate(list,sb.append(")"),open,close+1,n);
            sb.deleteCharAt(sb.length()-1);
        }

    }
}

package mar;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class RomantoInteger {
    public static Map<Character, Integer> map;

    public static void main(String[] args) {
        map = getMapping();
//        String s = "XIV";
//        String s = "MCMXCIV";
        String s = "VIII";
        System.out.println(romanToInt(s));
        System.out.println(Solution.romanToInt(s));
    }

    public static int romanToInt(String s) {
        Stack<Integer> stack = new Stack<>();
        map = getMapping();
        int currentNumber = map.get(s.charAt(0));

        stack.push(currentNumber);
        int subtractValue = 0;
        for (int i = 1; i < s.length(); i++) {
            currentNumber = map.get(s.charAt(i));
            if (currentNumber > stack.peek()) {
                //pop and value to be subtracted
                while (!stack.isEmpty() && currentNumber > stack.peek()) {
                    subtractValue += stack.pop();
                }
            }
            stack.push(currentNumber);
        }
        int positiveValue = 0;
        //process stack
        while (!stack.isEmpty()) {
            positiveValue += stack.pop();
        }
        return positiveValue - subtractValue;
    }

    public static Map<Character, Integer> getMapping() {
        if (map != null)
            return map;
        map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);
        return map;
    }

    //submitted
    private static class Solution {
        public static int romanToInt(String s) {
            map = getMapping();
            int lastNumber = 0;
            int currentNumber = 0;
            int totalValue = 0;
            int subtractValue = 0;
            for (int i = 0; i < s.length(); i++) {
                currentNumber = map.get(s.charAt(i));
                if (lastNumber < currentNumber) {
                    //large value found subtraction
                    totalValue += currentNumber - 2 * subtractValue;
                    subtractValue = currentNumber;
                    lastNumber = currentNumber;
                } else if (lastNumber == currentNumber) {
                    totalValue += currentNumber ;
                    subtractValue = 0;
                } else {
                    lastNumber = currentNumber;
                    subtractValue = currentNumber;
                    totalValue += currentNumber;
                }
            }
            return totalValue;
        }
    }

    //from leetcode
    private class Solution1 {
         int[] map =new int[256];
        {
            map['I']=1;
            map['V']=5;
            map['X']=10;
            map['L']=50;
            map['C']=100;
            map['D']=500;
            map['M']=1000;
        }
        public int romanToInt(String s) {
            char[] content = s.toCharArray();
            int result=0;
            int prevVal=9999; //to make it bigger then any in map initially
            //tokenize and summ up
            for(int i=0;i<content.length;i++){
                int val = map[content[i]];
                result = result+val;
                if(prevVal<val){
                    result=result-2*prevVal;
                }
                prevVal=val;
            }
            return result;
        }
    }
}



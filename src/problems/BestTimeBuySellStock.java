package problems;

public class BestTimeBuySellStock {
    public int maxProfit(int[] prices) {
        if (prices.length == 0 || prices.length == 1)
            return 0;
        int maxProfit = 0;
        int boughtStock = 0;

        for (int i = 1;i<prices.length;i++) {
            if (prices[boughtStock] < prices[i]) {
                //selling time
                if (maxProfit < prices[i] - prices[boughtStock])
                    maxProfit = prices[i] - prices[boughtStock];

            } else if (prices[boughtStock] > prices[i]) {
                //reset
                boughtStock = i;
            }
        }
        return maxProfit;
    }
}

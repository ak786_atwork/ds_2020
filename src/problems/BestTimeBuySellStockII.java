package problems;

//submitted on leetcode
public class BestTimeBuySellStockII {
    public static int maxProfit(int[] prices) {
        int maxProfit = 0;
        int stockBought = -1;

        int i = 1;
        for (i = 1;i<prices.length;i++) {
            if (prices[i-1] <= prices[i]) {
                if (stockBought == -1) {
                    stockBought = i-1;
                }
            } else {
                if (stockBought != -1) {
                    maxProfit += prices[i-1] - prices[stockBought];
                    stockBought = -1;
                }
            }
        }
        if (stockBought != -1) {
            maxProfit += prices[i-1] - prices[stockBought];
        }
        return maxProfit;
    }

    public static void main(String[] args) {
        int[] a = {7,1,5,3,6,4};
        System.out.println(maxProfit(a));
    }

    //leetcode solution
    public int maxProfit1(int[] prices) {

        int maxProfit = 0;

        for(int i = 1; i < prices.length; i++){

            if(prices[i] > prices[i-1]){
                maxProfit += (prices[i] - prices[i-1]);
            }
        }
        return maxProfit;
    }
}

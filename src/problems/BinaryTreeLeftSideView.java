package problems;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//submitted on geeksforgeeks correct
public class BinaryTreeLeftSideView {
    void leftView(Node1 root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) {
            System.out.println(list);
            return;
        }

        int count  = 1;
        Queue<Node1> queue = new LinkedList<>();
        queue.add(root);
        list.add(root.data);
        Node1 temp;

        while (!queue.isEmpty()) {
            temp = queue.poll();
            count--;
            if (temp.left != null)
                queue.add(temp.left);
            if (temp.right != null)
                queue.add(temp.right);

            if (count == 0) {
                if (!queue.isEmpty())
                    list.add(queue.peek().data);
                count = queue.size();
            }
        }
        
        System.out.println(list);
    }

/*    public static void main(String[] args) {
        problems.Node1 root = new problems.Node1(1);
        root.right = new problems.Node1(7);
//        root.left = new problems.Node1(99);
        root.right.left = new problems.Node1(9);

        (new problems.BinaryTreeLeftSideView()).leftView(root);
    }*/
    
}


class Node1
{
    int data;
    Node1 left, right;

    Node1(int item)
    {
        data = item;
        left = right = null;
    }
}
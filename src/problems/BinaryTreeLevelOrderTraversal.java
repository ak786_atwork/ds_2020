package problems;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeLevelOrderTraversal {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> lists = new ArrayList<>();
        if (root == null)
            return lists;

        int count  = 1;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        TreeNode temp;
        List<Integer> tempList = new ArrayList<>();

        while (!queue.isEmpty()) {
            temp = queue.poll();
            tempList.add(temp.val);
            count--;
            if (temp.left != null)
                queue.add(temp.left);
            if (temp.right != null)
                queue.add(temp.right);

            if (count == 0) {
                lists.add(tempList);
                tempList = new ArrayList<>();
                count = queue.size();
            }
        }

        return lists;
    }
}

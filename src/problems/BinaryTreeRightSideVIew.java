package problems;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//submitted on leetcode correct
public class BinaryTreeRightSideVIew {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
//        root.right = new problems.TreeNode(7);
        root.left = new TreeNode(99);
        root.left.right = new TreeNode(9);

        System.out.println((new BinaryTreeRightSideVIew()).rightSideView(root));
    }

    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if (root == null)
            return list;

        int count  = 1;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        TreeNode temp;

        while (!queue.isEmpty()) {
            temp = queue.poll();
            count--;
            if (temp.left != null)
                queue.add(temp.left);
            if (temp.right != null)
                queue.add(temp.right);

            if (count == 0) {
                list.add(temp.val);
                count = queue.size();
            }
        }

        return list;
    }

    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> lists = new ArrayList<>();
        if (root == null)
            return lists;

        int count  = 1;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        TreeNode temp;
        List<Integer> tempList = new ArrayList<>();

        while (!queue.isEmpty()) {
            temp = queue.poll();
            tempList.add(temp.val);
            count--;
            if (temp.left != null)
                queue.add(temp.left);
            if (temp.right != null)
                queue.add(temp.right);

            if (count == 0) {
                lists.add(tempList);
                tempList = new ArrayList<>();
                count = queue.size();
            }
        }

        return lists;
    }
}

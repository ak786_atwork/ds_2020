package problems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//submitted on leetcode
public class CopyListWithRandomPointer {

    public static void main(String[] args) {
        PNode head = new PNode(1);
        head.next = new PNode(2);
        head.next.random = head;
        head.random = null;

        copyRandomList(head);
    }

    //list modification not allowed
    public static PNode copyRandomList1(PNode head) {
        if (head == null)
            return null;

        int size = 0;
        PNode head2 = null;
        PNode temp = head;
        PNode lastPtr = head;
        PNode lastPtr2 = null;

        List<PNode> list = new ArrayList<>();

        while (temp != null) {
            //create new node
            PNode node = new PNode(temp.val);
            if (head2 == null) {
                head2 = node;
            } else {
                lastPtr2.next = node;
            }

            lastPtr2 = node;
            lastPtr = temp;
            temp = temp.next;
            lastPtr.next = node;

            list.add(lastPtr);
        }

        temp = head2;
        int currentIndex = 0;
        while (temp != null) {
            if (list.get(currentIndex).random != null)
                temp.random = list.get(currentIndex).random.next;
            currentIndex++;
            temp = temp.next;
        }

        return head2;
    }

    //100% faster
    public static PNode copyRandomList(PNode head) {
        if (head == null)
            return null;

        int size = 0;
        PNode head2 = null;
        PNode temp = head;
        PNode lastPtr = head;
        PNode lastPtr2 = null;
        int currentIndex = 0;

        List<PNode> list = new ArrayList<>();
        Map<PNode, Integer> oldNodesMapping = new HashMap<>();

        while (temp != null) {
            //create new node
            PNode node = new PNode(temp.val);
            if (head2 == null) {
                head2 = node;
            } else {
                lastPtr2.next = node;
            }

            oldNodesMapping.put(temp, currentIndex);

            lastPtr2 = node;
            temp = temp.next;
            list.add(node);
            currentIndex++;
        }

        PNode cPtr = head2;
        temp = head;
        currentIndex = 0;
        while (temp != null) {
            if (oldNodesMapping.containsKey(temp.random)) {
                cPtr.random = list.get(oldNodesMapping.get(temp.random));
            }
            currentIndex++;
            temp = temp.next;
            cPtr = cPtr.next;
        }

        return head2;
    }

    //without hashmap other people
    public PNode copyRandomList2(PNode head) {
        // put new nodes next after old nodes, connecting old nodes to new nodes next
        PNode curr = head;
        while (curr != null) {
            PNode tmpNext = curr.next;
            PNode newNext = new PNode(curr.val);
            curr.next = newNext;
            newNext.next = tmpNext;
            curr = tmpNext;
        }

        // connect randoms of new nodes
        curr = head;
        while (curr != null) {
            if (curr.random != null)
                curr.next.random = curr.random.next;
            else
                curr.next.random = null;
            curr = curr.next.next;
        }

        // recover old head and extract new head
        PNode dumHead = new PNode(-1);
        PNode currNewHead = dumHead;
        curr = head;

        while (curr != null) {
            currNewHead.next = curr.next;
            curr.next = curr.next.next;
            curr = curr.next;
            currNewHead = currNewHead.next;
        }

        return dumHead.next;
    }
}



class PNode {
    int val;
    PNode next;
    PNode random;

    public PNode(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
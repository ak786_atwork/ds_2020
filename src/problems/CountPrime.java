package problems;

public class CountPrime {

    public static void main(String[] args) {
        System.out.println((new CountPrime2()).countPrimes(0));
    }

    public int countPrimes(int n) {
        int count = 0;
        while (--n > 1) {
            count += isPrime(n);
//            System.out.println("n = "+n+" "+isPrime(n));
        }
        return count;
    }

    private int isPrime(int n) {
        for (int i = 2; i <= Math.sqrt(n);i++) {
            if (n % i == 0)
                return 0;
        }
        return 1;
    }

}

class CountPrime1 {

    public int countPrimes(int n) {
        int[] primeCounts = new int[n];
        for (int i = 2; i < n; i++) {
            primeCounts[i] = primeCounts[i-1] + isPrime(i);
            System.out.println("i = "+i+" prime = "+isPrime(i));
        }
        printArray(primeCounts);
        return primeCounts[n-1];
    }

    private void printArray(int[] primeCounts) {
        for (int i = 0;i<primeCounts.length;i++)
            System.out.print(""+primeCounts[i] +" ");
    }

    private int isPrime(int n) {
        int i = (int)  Math.sqrt(n-1);
//        if (i < 2)
//            return 1;
        for (; i <= Math.sqrt(n); i++) {
            if (n % i == 0)
                return 0;
        }
        return 1;
    }

}

//submitted on leetcode
class CountPrime2 {

    static int divisibleCount = 0;

    public int countPrimes(int n) {
        divisibleCount = 0;
        boolean[] primeCounts = new boolean[n];
        for (int i = 2; i*i < n; i++) {
            if (!primeCounts[i]) {
//                count++;
                markAllDivisible(primeCounts,i, n);
            }
        }
       /* for (int i = 2; i<n;i++) {
            if (!primeCounts[i])
                count++;
        }*/
//        printArray(primeCounts);
        System.out.println("n = "+n+" divisible = "+divisibleCount);
//        return n - 2 - divisibleCount;
        return Math.max((n - 2 - divisibleCount), 0);
    }

    private void markAllDivisible(boolean[] primeCounts, int i, int n) {
        for (int j = i*i; j < n; j += i) {
            if (!primeCounts[j])
                divisibleCount++;
//            System.out.println("j = "+j +"  divisible ="+ divisibleCount);
            primeCounts[j] = true;
        }
    }

    private void printArray(boolean[] primeCounts) {
        for (int i = 0;i<primeCounts.length;i++)
            System.out.print(""+primeCounts[i] +" ");
    }

    private int isPrime(int n) {
        int i = (int)  Math.sqrt(n-1);
//        if (i < 2)
//            return 1;
        for (; i <= Math.sqrt(n); i++) {
            if (n % i == 0)
                return 0;
        }
        return 1;
    }

}

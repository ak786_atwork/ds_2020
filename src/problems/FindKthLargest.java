package problems;

public class FindKthLargest {
    public static void main(String[] args) {
//        int[] nums = {2,1,1,2,1,4};
        int[] nums = {3,2,3,1,2,4,5,5,6,7,7,8,2,3,1,1,1,10,11,5,6,2,4,7,8,5,6};
        System.out.println((new FindKthLargest()).findKthLargest(nums,20));
        System.out.println((new HeapApproach()).findKthLargest(nums,20));
        System.out.println((new MinHeapApproach()).findKthLargest(nums,20));
    }
    public int findKthLargest(int[] nums, int k) {
        //You may assume k is always valid
        if (nums.length == 1)
            return nums[0];

        return partition(nums,0,nums.length-1,k);
    }

    private int partition(int[] nums, int start, int end, int kth) {
        if (start == end)
            return nums[start];
        int pivot = nums[end];
        int i = start;
        int j =start;
        for (;j<end;j++) {
            if (nums[j] < pivot) {
                //increment i and swap i and j
                swap(nums, i, j);
                i++;
            }
        }
        swap(nums,i,j);
        if (nums.length - i == kth) {
            return nums[i];
        } else {
            if (nums.length - i < kth) {
                //left side
                return partition(nums,start,i-1,kth);
            } else {
                //right side
                return partition(nums, i+1,end,kth);
            }
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}

//max heap
class HeapApproach{
    int heapsize = 1;
    public int findKthLargest(int[] nums, int k) {
        //You may assume k is always valid
        if (nums.length == 1)
            return nums[0];

        buildHeap(nums);

        //extract
        for (int i=0;i<k-1;i++){
            swap(nums, 0, heapsize-1);
            heapsize--;
            heapifyFromTop(nums,0);
        }

        return nums[0];//last extract;
    }

    private void buildHeap(int[] nums) {
        for (int i = 1;i<nums.length;i++) {
            heapsize++;
            heapify(nums,(i-1)/2);
        }
    }

    private void heapify(int[] nums, int parent) {
        if (parent < 0)
            return;
        int leftChild = 2 * parent + 1;
        int rightChild = 2 * parent + 2;
        boolean shouldHeapifyAll = false;

        if (leftChild < heapsize && nums[leftChild] > nums[parent]) {
            swap(nums,parent,leftChild);
            shouldHeapifyAll = true;
        }

        if (rightChild < heapsize && nums[rightChild] > nums[parent]) {
            swap(nums,parent,rightChild);
            shouldHeapifyAll = true;
        }
        if (shouldHeapifyAll)
            heapify(nums,(parent-1)/2);
    }

    private void heapifyFromTop(int[] nums, int parent) {
        if (parent >= heapsize)
            return;
        int leftChild = 2 * parent + 1;
        int rightChild = 2 * parent + 2;
        boolean heapifyRight = false;
        boolean heapifyLeft = false;

        if (leftChild < heapsize && nums[leftChild] > nums[parent]) {
            swap(nums,parent,leftChild);
            heapifyLeft = true;
        }

        if (rightChild < heapsize && nums[rightChild] > nums[parent]) {
            swap(nums,parent,rightChild);
            heapifyRight = true;
        }
//        if (heapifyRight)
            heapifyFromTop(nums,rightChild);
//        else if (heapifyLeft)
            heapifyFromTop(nums, leftChild);

    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}


//min heap
class MinHeapApproach{
    int heapsize = 1;
    public int findKthLargest(int[] nums, int k) {
        //You may assume k is always valid
        if (nums.length == 1)
            return nums[0];

        buildHeap(nums,k);

        for(int i = k; i<nums.length; i++)
        {
            if(nums[i] > nums[0]){
                nums[0] = nums[i];
                heapifyFromTop(nums,0);
            }
        }

        //extract
   /*     for (int i=0;i<k-1;i++){
            swap(nums, 0, heapsize-1);
            heapsize--;
            heapifyFromTop(nums,0);
        }*/

        return nums[0];//last extract;
    }

    private void buildHeap(int[] nums, int k) {
        for (int i = 1;i<k;i++) {
            heapsize++;
            heapify(nums,(i-1)/2);
        }
    }

    private void heapify(int[] nums, int parent) {
        if (parent < 0)
            return;
        int leftChild = 2 * parent + 1;
        int rightChild = 2 * parent + 2;
        boolean shouldHeapifyAll = false;

        if (leftChild < heapsize && nums[leftChild] < nums[parent]) {
            swap(nums,parent,leftChild);
            shouldHeapifyAll = true;
        }

        if (rightChild < heapsize && nums[rightChild] < nums[parent]) {
            swap(nums,parent,rightChild);
            shouldHeapifyAll = true;
        }
        if (shouldHeapifyAll)
            heapify(nums,(parent-1)/2);
    }

    private void heapifyFromTop(int[] nums, int parent) {
        if (parent >= heapsize)
            return;
        int leftChild = 2 * parent + 1;
        int rightChild = 2 * parent + 2;
        boolean heapifyRight = false;
        boolean heapifyLeft = false;

        if (leftChild < heapsize && nums[leftChild] < nums[parent]) {
            swap(nums,parent,leftChild);
            heapifyLeft = true;
        }

        if (rightChild < heapsize && nums[rightChild] < nums[parent]) {
            swap(nums,parent,rightChild);
            heapifyRight = true;
        }
//        if (heapifyRight)
        heapifyFromTop(nums,rightChild);
//        else if (heapifyLeft)
        heapifyFromTop(nums, leftChild);

    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
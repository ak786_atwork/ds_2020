package problems;

import java.util.ArrayList;
import java.util.List;

public class GrayCode {
    public static void main(String[] args) {
        System.out.println((new GrayCode()).grayCode1(2));
    }
    public List<Integer> grayCode(int n) {
        if(n==0){
            List<Integer> result = new ArrayList<Integer>();
            result.add(0);
            return result;
        }

        List<Integer> result = grayCode(n-1);
        int numToAdd = 1<<(n-1);

        for(int i=result.size()-1; i>=0; i--){ //iterate from last to first
            result.add(numToAdd+result.get(i));
        }

        return result;
    }

    //from leetcode
    public List<Integer> grayCode1(int n) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 1 << n; i++) {
            System.out.println(i+"  i^i >> 1 = "+(i ^ i >> 1));
            result.add(i ^ i >> 1);
        }
        return result;
    }
}

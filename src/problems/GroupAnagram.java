package problems;

import java.util.*;

//sumitted on leetcode
public class GroupAnagram {

    public static void main(String[] args) {
        String[] strs = {"abc", "bca","tan","tany"};
        System.out.println((new GroupAnagram()).groupAnagrams(strs));
    }

    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> lists = new ArrayList<>();
        if (strs.length == 0)
            return lists;
        int listIndex = 0;
        boolean matched = false;
        HashMap<Integer, List<Group>> listHashMap = new HashMap<>();
        HashMap<Character, Integer> temp = new HashMap<>();
        //add first item to group

        for (int i = 0;i<strs.length;i++) {
            matched = false;
//            temp.clear();
            temp = prepareCharacterMap(strs[i]);
            if (listHashMap.containsKey(strs[i].length())) {
                for (Group g : listHashMap.get(strs[i].length())) {
                    if (matchChar(temp, g.map)) {
                        //group found add this item to that group and in result list
                        lists.get(g.index).add(strs[i]);
                        matched = true;
                        break;
                    }
                }
                if (!matched) {
                    //means this is having new set of characters, so creating a new group
                    listHashMap.get(strs[i].length()).add(new Group(listIndex,temp));
                    List<String> stringList  = new ArrayList<>();
                    stringList.add(strs[i]);
                    lists.add(listIndex, stringList);
                    listIndex++;
                }
            } else {
                //create a new group
                List<Group> groupList = new ArrayList<>();
                groupList.add(new Group(listIndex, temp));

                List<String> stringList  = new ArrayList<>();
                stringList.add(strs[i]);
                lists.add(listIndex, stringList);

                listHashMap.put(strs[i].length(), groupList);

                listIndex++;
            }
        }

//        System.out.println(lists.toString());
        return lists;
    }

    private boolean matchChar(HashMap<Character, Integer> map1, HashMap<Character, Integer> map2) {
//        System.out.println("map1 = "+map1.toString()+" map2 = "+map2.toString()+"  is equal = "+map1.equals(map2));
        return map1.equals(map2);
    }

    private HashMap<Character, Integer> prepareCharacterMap(String str) {
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i=0;i<str.length();i++) {
            if (!map.containsKey(str.charAt(i))) {
                map.put(str.charAt(i), 1);
            } else {
                map.put(str.charAt(i), map.get(str.charAt(i)) + 1);
            }
        }
//        System.out.println(map.toString());
        return map;
    }
}

class Group{
    int index;
    HashMap<Character, Integer> map;

    public Group(int index, HashMap<Character, Integer> map) {
        this.index = index;
        this.map = map;
    }
}


//from leetcode
class GroupAnagramSolution {
    public List<List<String>> groupAnagrams(String[] strs) {
        if (strs.length == 0) return new ArrayList();
        Map<String, List> ans = new HashMap<String, List>();
        int[] count = new int[26];
        for (String s : strs) {
            Arrays.fill(count, 0);
            for (char c : s.toCharArray()) count[c - 'a']++;

            StringBuilder sb = new StringBuilder("");
            for (int i = 0; i < 26; i++) {
                sb.append('#');
                sb.append(count[i]);
            }
            String key = sb.toString();
            if (!ans.containsKey(key)) ans.put(key, new ArrayList());
            ans.get(key).add(s);
        }
        return new ArrayList(ans.values());
    }
}
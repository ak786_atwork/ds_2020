package problems;

import java.util.HashSet;
import java.util.Set;

public class IntersectionOfTwoLinkedLists {
    public static void main(String[] args) {
        ListNode head1 = new ListNode(1);
        head1.next = new ListNode(2);
        ListNode common = new ListNode(5);
        common.next = new ListNode(6);
        ListNode head2 = common;

        head1.next.next = common;

        System.out.println((new IntersectionOfTwoLinkedLists()).getIntersectionNode1(head1,head2).val);
    }

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null)
            return null;

        Set<ListNode> set = new HashSet<>();

        while (headA != null && headB != null) {
            if (set.contains(headA)) {
                return headA;
            } else {
                set.add(headA);
            }
            if (set.contains(headB)) {
                return headB;
            } else {
                set.add(headB);
            }
            headA = headA.next;
            headB = headB.next;
        }

        headA = headA == null ? headB : headA;
        while (headA != null) {
            if (set.contains(headA)) {
                return headA;
            } else {
                set.add(headA);
            }
            headA = headA.next;
        }
        return null;
    }

    public ListNode getIntersectionNode1(ListNode head1, ListNode head2) {
        int len1 = findLength(head1);
        int len2 = findLength(head2);
        int diff = len1 - len2;

        if (diff == 0) {
            if (head1 == head2)
                return head1;
            else
                return null;
        } else if (diff > 0) {
            //len1 is larger, means move this to
            head1 = move(head1, diff);
            return findCommonNode(head1, head2);
        } else {
            // list2 is bigger
            head2 = move(head2, -diff);
            return findCommonNode(head1, head2);
        }
    }

    private ListNode findCommonNode(ListNode head1, ListNode head2) {
        while (head1 != null && head2 != null) {
            if (head1 == head2) {
                return head1;
            }
            head1 = head1.next;
            head2 = head2.next;
        }
        return null;
    }

    private int findLength(ListNode root) {
        int count = 0;
        while (root != null) {
            count++;
            root = root.next;
        }
        return count;
    }

    private ListNode move(ListNode root, int count) {
        while (count-- > 0) {
            root = root.next;
        }
        return root;
    }
}

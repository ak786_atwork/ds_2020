package problems;

import java.util.ArrayList;
import java.util.HashMap;

class Island {
    public static void main(String[] args) {
        char[][] grid = {{'1', '1', '1', '1', '0'},
                {'1', '1', '0', '1', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '0', '0', '0'}
        };

        char[][] grid1 = {{'1', '1', '0', '0', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '1', '0', '0'},
                {'0', '0', '0', '1', '1'}
        };

        char[][] grid2 = {};

        System.out.println((new Island()).numIslands(grid1));
    }

    public int numIslands(char[][] grid) {
        if (grid.length == 0)
            return 0;
        ArrayList<Position> queue = new ArrayList<>();
        int row = grid.length;
        int col = grid[0].length;
        int count = 0;
        int sizeCount = 0;
        boolean exit = false;
        HashMap<String, Boolean> hashMap = new HashMap<>();
        System.out.println("row = "+row+" col = "+col);
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                queue.clear();
                if (grid[i][j] == '1') {
                    count++;
                    queue.add(new Position(i, j));

//                    printQueue(queue);
                } else {
                    sizeCount++;
                }
                printQueue(queue);
                while (!queue.isEmpty()) {
                    sizeCount++;
                    Position position = queue.remove(0);
                    int rowNumber = position.i;
                    int colNumber = position.j;
                    grid[rowNumber][colNumber] = '0';
                    //add all adjacent
                    addAllAdjacent(rowNumber, colNumber, queue, grid, row, col);
//                    printQueue(queue);
                }
                System.out.println("==================================================="+sizeCount);
                if (sizeCount == row * col) {
//                    exit = true;
                    i = row;j=col;
                    System.out.println("exit");
                    break;
                }
            }
            if (exit)
                break;
        }
        return count;
    }

    private void printQueue(ArrayList<Position> queue) {
//        queue.forEach((position -> System.out.print(" "+position.i+""+position.j)));
//        System.out.println();
    }

    private void addAllAdjacent(int rowNumber, int colNumber, ArrayList<Position> queue, char[][] grid, int row, int col) {
        printQueue(queue);
        if (rowNumber - 1 >= 0) {
            checkAndAdd(queue, grid, rowNumber - 1, colNumber);
        }
        if (colNumber - 1 >= 0) {
            checkAndAdd(queue, grid, rowNumber, colNumber - 1);
        }
        if (colNumber + 1 < col) {
            checkAndAdd(queue, grid, rowNumber, colNumber + 1);
        }
        if (rowNumber + 1 < row) {
            checkAndAdd(queue, grid, rowNumber + 1, colNumber);
        }
        printQueue(queue);
    }

    private void checkAndAdd(ArrayList<Position> queue, char[][] grid, int rowNumber, int colNumber) {
        if (grid[rowNumber][colNumber] == '1')
            queue.add(new Position(rowNumber, colNumber));
    }
}

class Position {
    int i;
    int j;

    public Position(int i, int j) {
        this.i = i;
        this.j = j;
    }
}
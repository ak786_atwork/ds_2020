package problems;

import java.util.HashMap;


//submitted on leetcode
public class LRUCache {

    HashMap<Integer, DoublyLinkedListNode> nodeLooKUp;
    HashMap<DoublyLinkedListNode, Integer> keyLookUp;
    DoublyLinkedListNode head;
    DoublyLinkedListNode tail;
    int capacity;
    int currentSize = 0;

    public LRUCache(int capacity) {
        head = null;
        tail = null;
        this.capacity = capacity;
        nodeLooKUp = new HashMap<>();
        keyLookUp = new HashMap<>();
    }

    public int get(int key) {
        //System.out.println("before get key = "+key);
        traverseLinkedList();
        int res = -1;
        if (nodeLooKUp.containsKey(key)) {
            reorderToFront(key);
            res =  nodeLooKUp.get(key).data;
        }
        traverseLinkedList();
        //System.out.println("get key ends--------------");
        return res;
    }

    private void reorderToFront(int key) {
        if (head == null || head == nodeLooKUp.get(key))
            return;


        if (tail == nodeLooKUp.get(key)) {
            DoublyLinkedListNode lastNode = tail.previous;
            tail.previous = null;
            lastNode.next = null;
            tail.next = head;
            tail.next.previous = tail;
            head = tail;
            tail = lastNode;

        } else {
            // inner nodes
            DoublyLinkedListNode temp = nodeLooKUp.get(key);
            temp.previous.next = temp.next;
            temp.next.previous = temp.previous;

            //adding to head
            temp.previous = null;
            temp.next = head;
            temp.next.previous = temp;
            head = temp;
        }
    }

    private void traverseLinkedList() {
        DoublyLinkedListNode temp = head;
        //System.out.println();
        //System.out.print("forward = ");
        while (temp != null) {
            //System.out.print(temp.data + "-> ");
            temp = temp.next;
        }
        //System.out.println();
        //System.out.print("backward = ");

        temp = tail;
        while (temp != null) {
            //System.out.print(temp.data + "-> ");
            temp = temp.previous;
        }
        //System.out.println();
    }

    public void put(int key, int value) {
        //System.out.println("put call ----- key = "+key+" value = "+value);
        if (nodeLooKUp.containsKey(key)) {
            DoublyLinkedListNode node = nodeLooKUp.get(key);
            node.data = value;
            reorderToFront(key);
        } else {
            if (capacity == currentSize) {
                removeLRUElement();
            }
            insert(key, value);
        }
        traverseLinkedList();
        //System.out.println("put call ends --------");
    }

    private void removeLRUElement() {
        if (capacity == 1) {
            keyLookUp.clear();
            nodeLooKUp.clear();
            head = tail = null;
            return;
        }
        DoublyLinkedListNode lastNode = tail.previous;
        lastNode.next = null;
        nodeLooKUp.remove(tail.data);
        tail = lastNode;
        currentSize--;
    }

    private void insert(int key, int value) {
        DoublyLinkedListNode node = new DoublyLinkedListNode(value);
        nodeLooKUp.put(key, node);

        //add this to linked list
        if (head == null) {
            tail = node;
            head = node;
        } else {
            //add the node at front
            node.next = head;
            head.previous = node;
            head = node;
        }
        currentSize++;
    }
}

//submitted on leetcode
 class LRUCache1 {

    HashMap<Integer, DoublyLinkedListNode> nodeLooKUp;
    DoublyLinkedListNode head;
    DoublyLinkedListNode tail;
    int capacity;
    int currentSize = 0;

    public LRUCache1(int capacity) {
        head = null;
        tail = null;
        this.capacity = capacity;
        nodeLooKUp = new HashMap<>();
    }

    public int get(int key) {
        //System.out.println("before get key = "+key);
        traverseLinkedList();
        int res = -1;
        if (nodeLooKUp.containsKey(key)) {
            reorderToFront(key);
            res =  nodeLooKUp.get(key).data;
        }
        traverseLinkedList();
        //System.out.println("get key ends--------------");
        return res;
    }

    private void reorderToFront(int key) {
        if (head == null || head == nodeLooKUp.get(key))
            return;


        if (tail == nodeLooKUp.get(key)) {
            DoublyLinkedListNode lastNode = tail.previous;
            tail.previous = null;
            lastNode.next = null;
            tail.next = head;
            tail.next.previous = tail;
            head = tail;
            tail = lastNode;

        } else {
            // inner nodes
            DoublyLinkedListNode temp = nodeLooKUp.get(key);
            temp.previous.next = temp.next;
            temp.next.previous = temp.previous;

            //adding to head
            temp.previous = null;
            temp.next = head;
            temp.next.previous = temp;
            head = temp;
        }
    }

    private void traverseLinkedList() {
        DoublyLinkedListNode temp = head;
        //System.out.println();
        //System.out.print("forward = ");
        while (temp != null) {
            //System.out.print(temp.data + "-> ");
            temp = temp.next;
        }
        //System.out.println();
        //System.out.print("backward = ");

        temp = tail;
        while (temp != null) {
            //System.out.print(temp.data + "-> ");
            temp = temp.previous;
        }
        //System.out.println();
    }

    public void put(int key, int value) {
        //System.out.println("put call ----- key = "+key+" value = "+value);
        if (nodeLooKUp.containsKey(key)) {
            DoublyLinkedListNode node = nodeLooKUp.get(key);
            node.data = value;
            reorderToFront(key);
        } else {
            if (capacity == currentSize) {
                removeLRUElement();
            }
            insert(key, value);
        }
        traverseLinkedList();
        //System.out.println("put call ends --------");
    }

    private void removeLRUElement() {
        if (capacity == 1) {
            nodeLooKUp.clear();
            head = tail = null;
            return;
        }
        DoublyLinkedListNode lastNode = tail.previous;
        lastNode.next = null;
        nodeLooKUp.remove(tail.key);
        tail = lastNode;
        currentSize--;
    }

    private void insert(int key, int value) {
        DoublyLinkedListNode node = new DoublyLinkedListNode(key,value);
        nodeLooKUp.put(key, node);

        //add this to linked list
        if (head == null) {
            tail = node;
            head = node;
        } else {
            //add the node at front
            node.next = head;
            head.previous = node;
            head = node;
        }
        currentSize++;
    }
}

class DoublyLinkedListNode {
    int data;
    int key;
    DoublyLinkedListNode next;
    DoublyLinkedListNode previous;

    public DoublyLinkedListNode(int data) {
        this.data = data;
    }

    public DoublyLinkedListNode(int key, int value) {
        this.key = key;
        this.data = value;
    }
}

class Driver {
    public static void main(String[] args) {
        LRUCache cache = new LRUCache( 1 /* capacity */ );

//        ["LRUCache","put","get","put","get","get"]
//[[1],[2,1],[2],[3,2],[2],[3]]

        cache.put(2, 1);
        System.out.println(cache.get(2));
        cache.put(3, 2);
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));

        System.out.println("-------------");
        LRUCache1 cache1 = new LRUCache1( 2 /* capacity */ );

//["LRUCache","put","put","get","get","put","get","get","get"]
//[[2],[2,1],[3,2],[3],[2],[4,3],[2],[3],[4]]

        cache1.put(2, 1);
        cache1.put(3, 2);
        System.out.println(cache1.get(3));
        System.out.println(cache1.get(2));
        cache1.put(4,3);

        System.out.println(cache1.get(2));
        System.out.println(cache1.get(3));
        System.out.println(cache1.get(4));

    }
}


/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */

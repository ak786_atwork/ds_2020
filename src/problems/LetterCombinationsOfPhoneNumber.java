package problems;

import java.util.*;

//submitted on leetcode
public class LetterCombinationsOfPhoneNumber {
    String[] map = {"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

    public static void main(String[] args) {
        System.out.println((new LSolution1()).letterCombinations("23").toString());
    }

    public List<String> letterCombinations(String digits) {
        List<String> list = new ArrayList<>();

        for (char ch : digits.toCharArray()) {
            list = addCombinations(map[ch - '2'], list);
        }
        return list;
    }

    private List<String> addCombinations(String s, List<String> list) {
        if (list.isEmpty()) {
            for (int i = 0; i < s.length(); i++) {
                list.add(String.valueOf(s.charAt(i)));
            }
            return list;
        } else {
            List<String> newList = new ArrayList<>();
            for (int i = 0; i < s.length(); i++) {
                for (String w : list) {
                    newList.add(w + s.charAt(i));
                }
            }
            return newList;
        }
    }
}

//slight improvement
class LSolution {
    String[] map = {"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    public List<String> letterCombinations(String digits) {
        List<String> list = new ArrayList<>();
        if (digits.isEmpty())
            return list;

        String s = map[digits.charAt(0) - '2'];
        for (int i = 0; i < s.length(); i++) {
            list.add(String.valueOf(s.charAt(i)));
        }

        for (int i = 1; i<digits.length();i++) {
            list = addCombinations(map[digits.charAt(i) - '2'], list);
        }
        return list;
    }

    private List<String> addCombinations(String s, List<String> list) {

        List<String> newList = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            for (String w : list) {
                newList.add(w + s.charAt(i));
            }
        }
        return newList;

    }
}

class LSolution1 {
    String[] map = {"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    public List<String> letterCombinations(String digits) {
        List<String> list = new ArrayList<>();
        if (digits.isEmpty())
            return list;



        addCombinations(0, "", list, digits);
        return list;
    }

    private void addCombinations(int currentIndex, String currentString, List<String> resultList, String digits) {
        if (currentIndex == digits.length()) {
            resultList.add(currentString);
            return;
        }
        String letters = map[digits.charAt(currentIndex) - '2'];
        for (int i = 0;i<letters.length();i++) {
            addCombinations(currentIndex+1, currentString+letters.charAt(i),resultList,digits);
        }
    }
}

//from leetcode
class FinalSolution {
    Map<String, String> phone = new HashMap<String, String>() {{
        put("2", "abc");
        put("3", "def");
        put("4", "ghi");
        put("5", "jkl");
        put("6", "mno");
        put("7", "pqrs");
        put("8", "tuv");
        put("9", "wxyz");
    }};

    List<String> output = new ArrayList<String>();

    public void backtrack(String combination, String next_digits) {
        // if there is no more digits to check
        if (next_digits.length() == 0) {
            // the combination is done
            output.add(combination);
        }
        // if there are still digits to check
        else {
            // iterate over all letters which map
            // the next available digit
            String digit = next_digits.substring(0, 1);
            String letters = phone.get(digit);
            for (int i = 0; i < letters.length(); i++) {
                String letter = phone.get(digit).substring(i, i + 1);
                // append the current letter to the combination
                // and proceed to the next digits
                backtrack(combination + letter, next_digits.substring(1));
            }
        }
    }

    public List<String> letterCombinations(String digits) {
        if (digits.length() != 0)
            backtrack("", digits);
        return output;
    }
}
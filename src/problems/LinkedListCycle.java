package problems;

public class LinkedListCycle {

    //submitted to leetcode worked  in one go
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        ListNode sptr, fptr,temp;
        sptr = head;
        fptr = head.next.next;

        while (fptr != null && fptr.next != null) {
            if (sptr == fptr)
                return true;

            sptr = sptr.next;
            fptr = fptr.next.next;
        }
        return false;
    }
}

class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
          val = x;
          next = null;
      }
  }

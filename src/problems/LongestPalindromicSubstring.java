package problems;

import java.util.Arrays;
import java.util.HashMap;

public class LongestPalindromicSubstring {

    public static void main(String[] args) {
        String s = "babadada";
//        String s = "madame";

//        (new problems.LongestPalindromicSubstring()).find(s);
        System.out.println((new LPS1()).longestPalindrome(s));
    }

    String substring = "";

    public String find(String s) {
        checkPalindrome(s, 0, s.length() - 1);
        System.out.println(substring);
        return substring;
    }

    public boolean checkPalindrome(String s, int i, int j) {
        if (i > j)
            return true;
        if (i == j) {
            findSubstring(s, i, j + 1);
            return true;
        }
        checkPalindrome(s, i + 1, j);
        checkPalindrome(s, i, j - 1);
        if (s.charAt(i) == s.charAt(j)) {
            if (checkPalindrome(s, i + 1, j - 1)) {
                findSubstring(s, i, j + 1);
                return true;
            }
        }

        return false;
    }

    private void findSubstring(String s, int i, int j) {
        System.out.println("---" + substring + " len =" + substring.length() + "   i = " + i + " j = " + j + " " + s.substring(i, j));
        if (substring.length() < j - i) {
//            System.out.println("---"+substring +" len =" +substring.length() + "   i = "+i+" j = "+j );
            substring = s.substring(i, j);

        }
    }

}

// submitted on leetcode
class LPS {
    int start = 0;
    int end = 0;

    public String longestPalindrome(String s) {
        if (s.isEmpty())
            return "";
        start = 0;
        end = 0;
        int[][] dp = new int[s.length()][s.length()];
        // Fill each row with 10.
        for (int[] row : dp)
            Arrays.fill(row, -1);
        check(s, 0, s.length() - 1, dp);

        return s.substring(start, end+1);
    }

    private int check(String s, int start, int end, int[][] dp) {
        if (start > end)
            return 0;
        if (start == end)
            return dp[start][end] = 1;

        if (dp[start][end] != -1)
            return dp[start][end];
        int len1 = 0;
        //calculate
        if (s.charAt(start) == s.charAt(end)) {
            if (check(s, start + 1, end - 1, dp) == (end - start - 1)) {
                updateSubString(start, end);
                len1 = end - start + 1;
            }
        }
        int len2 = check(s, start + 1, end, dp);
        int len3 = check(s, start, end - 1, dp);
        dp[start][end] = Math.max(len1, Math.max(len2, len3));
        return dp[start][end];
    }

    private void updateSubString(int i, int j) {
        if (j - i > end - start) {
            end = j;
            start = i;
        }
    }
}

//tle
class LPS1 {
    int start = 0;
    int end = 0;

    public String longestPalindrome(String s) {
        if (s.isEmpty())
            return "";
        start = 0;
        end = 0;

        check(s, 0, s.length() - 1, new HashMap<String, Integer>());

        return s.substring(start, end+1);
    }

    private int check(String s, int start, int end, HashMap<String, Integer> map) {
        if (start > end)
            return 0;
        if (start == end) {
            map.put(start+""+end, 1);
            return 1;
        }

        if (map.containsKey(start+""+end))
            return map.get(start+""+end);
        int len1 = 0;
        //calculate
        if (s.charAt(start) == s.charAt(end)) {
            if (check(s, start + 1, end - 1, map) == (end - start - 1)) {
                updateSubString(start, end);
                len1 = end - start + 1;
            }
        }
        int len2 = check(s, start + 1, end, map);
        int len3 = check(s, start, end - 1, map);
        map.put(start+""+end, Math.max(len1, Math.max(len2, len3)));
        return map.get(start+""+end);
    }

    private void updateSubString(int i, int j) {
        if (j - i > end - start) {
            end = j;
            start = i;
        }
    }
}

//from leetcode
class LPSSolution {
    public String longestPalindrome(String s) {
        if (s == null || s.length() < 1) return "";
        int start = 0, end = 0;
        for (int i = 0; i < s.length(); i++) {
            int len1 = expandAroundCenter(s, i, i);
            int len2 = expandAroundCenter(s, i, i + 1);
            int len = Math.max(len1, len2);
            if (len > end - start) {
                start = i - (len - 1) / 2;
                end = i + len / 2;
            }
        }
        return s.substring(start, end + 1);
    }

    private int expandAroundCenter(String s, int left, int right) {
        int L = left, R = right;
        while (L >= 0 && R < s.length() && s.charAt(L) == s.charAt(R)) {
            L--;
            R++;
        }
        return R - L - 1;
    }
}

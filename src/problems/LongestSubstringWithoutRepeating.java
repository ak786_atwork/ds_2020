package problems;

import java.util.HashMap;

public class LongestSubstringWithoutRepeating {

    public static void main(String[] args) {
        String s = "abbaui";
        System.out.println((new LongestSubstringWithoutRepeating()).lengthOfLongestSubstring(s));
    }

    //submitted on leetcode
    public int lengthOfLongestSubstring(String s) {
        int currentLength = 0;
        int maxLength = 0;
        int subStringBegIndex = 0;
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if (map.containsKey(s.charAt(i))) {
                if (map.get(s.charAt(i)) >= subStringBegIndex) {
                    if (currentLength > maxLength)
                        maxLength = currentLength;
                    currentLength = i - map.get(s.charAt(i));
                    subStringBegIndex = map.get(s.charAt(i)) + 1;
                } else {
//                    System.out.println("---");
                    currentLength++;
                }
                map.put(s.charAt(i), i);
            } else {
                currentLength++;
                map.put(s.charAt(i), i);
            }
        }
        if (currentLength > maxLength)
            maxLength = currentLength;
        return maxLength;
    }


}

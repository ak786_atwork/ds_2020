package problems;

//submitted on leetcode
public class LowestCommonAncestorBST {

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if ((root.val <= p.val && root.val >= q.val) || (root.val <= q.val && root.val >= p.val))
            return root;

        if (root.val >= p.val && root.val >= q.val)
            return lowestCommonAncestor(root.left, p, q);
        else
            return lowestCommonAncestor(root.right, p, q);
    }

    public TreeNode lowestCommonAncestor1(TreeNode root, TreeNode p, TreeNode q) {

        while (!((root.val <= p.val && root.val >= q.val) || (root.val <= q.val && root.val >= p.val))) {
            if (root.val >= p.val && root.val >= q.val)
                root = root.left;
            else
                root = root.right;
        }
        return root;
    }

}

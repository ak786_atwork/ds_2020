package problems;

//submitted on leetcode
public class LowestCommonAncestorBinaryTree {
    boolean firstFound = false;
    TreeNode target = null;
    TreeNode parent = null;
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        findNode(root, p, q);
        return parent;
    }

    private void findNode(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null)
            return;

        if (root == p || root == q) {
            firstFound = true;
            target = root == p ? q : p;
            if (explore(root))
                parent = root;
        } else {
            findNode(root.left, p, q);
            if (firstFound) {
                if (parent == null) {
                    if (explore(root.right)) {
                        parent = root;
                    }
                }
            } else {
                findNode(root.right,p,q);
            }
        }
    }

    private boolean explore(TreeNode root) {
        if (root == null)
            return false;
        return root == target || explore(root.left) || explore(root.right);
    }

    //trial only
    /*private problems.TreeNode findNode1(problems.TreeNode root, problems.TreeNode p, problems.TreeNode q, problems.TreeNode targetNode) {
        if (root == null)
            return null;
        problems.TreeNode parentNode = null;

        if (firstFound) {
            //explore the second one
            if (root == targetNode)
                return root;
            parentNode = findNode(root.left,p,q)
        } else {
            if (root == p) {
                //explore left and right
            } else if ( root == q) {
                //explore left and right of q
            } else {
                problems.TreeNode left = findNode(root.left, p, q, false);
                if (left == null)
                    return findNode(root.right, p, q, false);
            }
        }
    }*/
}

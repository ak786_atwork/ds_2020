package problems;

public class MaximumProductSubarray {

    public static void main(String[] args) {
        int[] nums = {2,3,-2,4};
        int[] nums1 = {0,2};
        int[] nums2 = {1,0,-1,2,3,-5,-2};

//        System.out.println((new problems.MaximumProductSubarray()).maxProduct(nums2));
        System.out.println((new MaximumProductSubarray()).maxProduct3(nums));
    }

    public int maxProduct(int[] nums) {
        int maxProduct = nums[0];
        int[][] matrix = new int[nums.length][nums.length];

        int row = 0;
        int col = 0;

        while (true) {
            if (row == col) {
                matrix[row][col] = nums[row];
                maxProduct = checkMax(matrix[row][col], maxProduct);
            } else {
                matrix[row][col] = matrix[row][col-1] * nums[col];
                matrix[col][row] = matrix[row][col];
                maxProduct = checkMax(matrix[row][col], maxProduct);
            }
            col++;
            if (col == nums.length) {
                row = row+1;
                if (row == nums.length)
                    break;
                col = row;
            }
        }
        printMatrix(matrix);
        return maxProduct;
    }

    private void printMatrix(int[][] matrix) {
        for (int i = 0;i<matrix.length;i++) {
            for (int j =0;j<matrix.length; j++) {
                System.out.print(matrix[i][j] +" ");
            }
            System.out.println();
         }
    }

    public int maxProduct1(int[] nums) {
        int maxProduct = nums[0];
        for (int i=1; i<nums.length;i++)
            nums[i] = nums[i-1] * nums[i];

        for (int i = 0; i < nums.length;i++) {
            for (int j = i; j < nums.length; j++ ) {
                if (i == 0) {
                    maxProduct = checkMax(nums[j],maxProduct);
                } else {
                    maxProduct = checkMax(nums[j]/nums[i-1],maxProduct);
                }
            }
        }
        return maxProduct;
    }

    public int maxProduct2(int[] nums) {
        int maxProduct = nums[0];
        int temp = 1;
        int[] cummalativeSum = new int[nums.length];
        cummalativeSum[0] = nums[0];
        for (int i=1; i<nums.length;i++)
            cummalativeSum[i] = cummalativeSum[i-1] * nums[i];

        for (int i = 0; i < nums.length;i++) {
            for (int j = i; j < nums.length; j++ ) {
                if (i == 0) {
                    maxProduct = checkMax(cummalativeSum[j],maxProduct);
                } else if (i==j) {
                    maxProduct = checkMax(nums[j], maxProduct);
                }
                else {
                    temp = (cummalativeSum[i-1] == 0) ? 1 : cummalativeSum[i-1];
                    maxProduct = checkMax(cummalativeSum[j]/temp ,maxProduct);
                }
            }
        }
        printArray(cummalativeSum);
        return maxProduct;
    }

    //copied best
    public int maxProduct3(int[] nums) {
        int n = nums.length;
        int min = nums[0];
        int max = nums[0];
        int res = nums[0];
//        {1,0,-1,2,3,-5,-2};
        for(int i = 1 ; i < n ; i++)
        {
            int temp_max = max;
            int temp_min = min;
            max = Math.max(nums[i], Math.max(nums[i]*temp_max, nums[i]*temp_min));
            min = Math.min(nums[i], Math.min(nums[i]*temp_max, nums[i]*temp_min));
            res = Math.max(res, Math.max(max, min));
            System.out.println("max = "+max+" min = "+min+" result = "+res);
        }
        return res;
    }

    private void printArray(int[] array) {
        System.out.println();
        for (int i=0;i<array.length;i++)
            System.out.print(array[i] + " ");
        System.out.println();
    }

    private int checkMax(int number, int maxProduct) {
        if (number > maxProduct)
            return number;
        return maxProduct;
    }
}

package problems;

import java.util.Comparator;
import java.util.PriorityQueue;

//both working submitted on leetcode
public class MergeKSortedLinkedList {
    public static void main(String[] args) {
        ListNode node = new ListNode(1);
        node.next = new ListNode(6);


        ListNode node1 = new ListNode(0);
        node1.next = new ListNode(2);
        node1.next.next = new ListNode(19);

        printNodes(node);
        printNodes(node1);
        ListNode[] listNodes = new ListNode[2];
        listNodes[0] = node;
        listNodes[1] = node1;
        printNodes((new MergeSortApproach()).mergeKLists(listNodes));
//        printNodes((new problems.MergeKSortedLinkedList()).mergeKLists(listNodes));

    }

    private static void printNodes(ListNode head) {
        while (head != null) {
            System.out.print(head.val+" -> ");
            head = head.next;
        }
        System.out.println();
    }

    private Comparator<ListNode> comparator = new Comparator<ListNode>() {
        @Override
        public int compare(ListNode o1, ListNode o2) {
            return o1.val - o2.val;
        }
    };

    public ListNode mergeKLists(ListNode[] lists) {
        PriorityQueue<ListNode> minHeap = new PriorityQueue<ListNode>(comparator);

        //init minheap
        for (int i=0;i<lists.length;i++) {
            if (lists[i] != null) {
                minHeap.add(lists[i]);
            }
        }

        ListNode root = null;
        ListNode tail = null;
        ListNode temp = null;

        while (!minHeap.isEmpty()) {
            temp = minHeap.poll();
            if (temp.next != null)
                minHeap.add(temp.next);
            if (root == null) {
                root = temp;
                tail = temp;
            } else {
                tail.next = temp;
                tail = temp;
            }
        }

        return root;
    }

}

class MergeSortApproach {
    public ListNode mergeKLists(ListNode[] lists) {
        return mergeSort(lists,0,lists.length-1);
    }

    public ListNode mergeSort(ListNode[] lists, int beg, int end) {
        if (beg == end)
            return lists[beg];
        int mid = (beg + end)/2;
        ListNode leftListHead = mergeSort(lists, beg, mid);
        ListNode rightListHead =  mergeSort(lists, mid + 1, end);

        return merge(lists, leftListHead, rightListHead);
    }

    private ListNode merge(ListNode[] lists, ListNode leftListHead, ListNode rightListHead) {
        if (leftListHead == null && rightListHead == null)
            return null;
        else if (leftListHead == null)
            return rightListHead;
        else if (rightListHead== null)
            return leftListHead;

        ListNode head = null;
        ListNode tail = null;

        while (leftListHead != null && rightListHead != null) {
            if (leftListHead.val < rightListHead.val) {
                if (head == null) {
                    head = leftListHead;
                    tail = leftListHead;
                } else {
                    tail.next = leftListHead;
                    tail = leftListHead;
                }
                leftListHead = leftListHead.next;
            } else {
                if (head == null) {
                    head = rightListHead;
                    tail = rightListHead;
                } else {
                    tail.next = rightListHead;
                    tail = rightListHead;
                }
                rightListHead = rightListHead.next;
            }
        }

        leftListHead = leftListHead == null ? rightListHead : leftListHead;
        tail.next = leftListHead;
        return head;
    }
}


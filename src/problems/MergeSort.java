package problems;

public class MergeSort {

    public static void main(String[] args) {
        int[] nums = {1,4,6,1,9,0,-11};
        (new MergeSort()).mergeSort(nums,0,nums.length-1);

        printArray(nums);
//        System.out.println(nums);
    }

    private static void printArray(int[] nums) {
        System.out.println();
        for (int i=0;i<nums.length;i++)
            System.out.print(nums[i] +" ");
    }

    public void mergeSort(int[] nums, int beg, int end) {
        if (beg == end)
            return;
        int mid = (beg + end)/2;
        mergeSort(nums, beg, mid);
        mergeSort(nums, mid + 1, end);

        merge(nums, beg, mid, end);
    }

    private void merge(int[] nums, int beg, int mid, int end) {
        int[] leftArray = new int[mid-beg+1];
        int[] rightArray = new int[end-mid];

        copyArray(nums, leftArray, beg, mid);
        copyArray(nums, rightArray, mid+1, end);

        int left=0, right=0;
        while (left < leftArray.length && right < rightArray.length) {
            if (leftArray[left] < rightArray[right]) {
                nums[beg++] = leftArray[left++];
            } else {
                nums[beg++] = rightArray[right++];
            }
        }

        leftArray = left < leftArray.length ? leftArray : rightArray;
        left = left < leftArray.length ?  left : right;

        while (left<leftArray.length) {
            nums[beg++] = leftArray[left++];
        }
    }

    private void copyArray(int[] nums, int[] array, int start, int end) {
        int j=0;
        for (int i = start;i<=end;i++) {
            array[j++] = nums[i];
        }
    }

}


package problems;

public class MergeTwoSortedLinkedList {

    public static void main(String[] args) {
        ListNode node = new ListNode(1);
        node.next = new ListNode(6);


        ListNode node1 = new ListNode(0);
        node1.next = new ListNode(2);
        node1.next.next = new ListNode(19);

        printNodes(node);
        printNodes(node1);

        printNodes((new MergeTwoSortedLinkedList()).mergeTwoLists(node, node1));

    }


    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null)
            return null;
        else if (l1 == null)
            return l2;
        else if (l2== null)
            return l1;

        ListNode temp = null;
        ListNode newRootHead = null;
        ListNode newRootTail = null;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                if (newRootHead == null) {
                    newRootHead = l1;
                    newRootTail = l1;
                } else {
                    newRootTail.next = l1;
                    newRootTail = l1;
                }
                l1 = l1.next;
            } else {
                if (newRootHead == null) {
                    newRootHead = l2;
                    newRootTail = l2;
                } else {
                    newRootTail.next = l2;
                    newRootTail = l2;
                }
                l2= l2.next;
            }
        }

        l1 = l1 == null ? l2 : l1;
        newRootTail.next = l1;

        return newRootHead;
    }


    private static void printNodes(ListNode head) {
        while (head != null) {
            System.out.print(head.val+" -> ");
            head = head.next;
        }
        System.out.println();
    }

}

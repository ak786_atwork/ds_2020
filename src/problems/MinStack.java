package problems;

import java.util.Stack;

public class MinStack {

    public static void main(String[] args) {
        MinStack1 minStack = new MinStack1();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        minStack.getMin();
        minStack.pop();
        minStack.top();
        minStack.getMin();
    }

    /** initialize your data structure here. */
    Stack<Integer> stack;
    Stack<Integer> minStack;

    public MinStack() {
        stack = new Stack<>();
        minStack = new Stack<>();
    }

    public void push(int x) {
        stack.push(x);
        if (minStack.isEmpty() || minStack.peek() > x) {
            minStack.push(x);
        } else {
            minStack.push(minStack.peek());
        }
    }

    public void pop() {
        if (!minStack.isEmpty()) {
            minStack.pop();
            stack.pop();
        }
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return minStack.peek();
    }
}

//submitted to leetcode working
class MinStack1 {
    Node root;

    public MinStack1() {
        root = null;
    }

    public void push(int x) {
        if (root == null) {
            root = new Node(x,x,null);
        } else {
            Node node = new Node(Math.min(root.min, x), x, root);
            root = node;
        }
    }

    public void pop() {
        root = root.next;
    }

    public int top() {
        return root.number;
    }

    public int getMin() {
        return root.min;
    }
}

class Node {
    int min;
    int number;
    Node next;

    public Node(int min, int number, Node node) {
        this.min = min;
        this.number = number;
        this.next = node;
    }

}

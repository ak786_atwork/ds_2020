package problems;

import java.util.HashMap;
import java.util.Stack;

public class NextGreaterElement {

    //i did, bruteforce only, i didn't get the approach
    public int[] nextGreaterElement1(int[] nums1, int[] nums2) {
        int[] result = new int[nums1.length];
        boolean greaterFound = false;
        boolean numberFound = false;
        for(int i= 0;i<nums1.length;i++) {
            greaterFound = false;
            numberFound = false;
            for(int j = 0;j<nums2.length;j++) {
                if (!numberFound) {
                    if (nums1[i] == nums2[j])
                        numberFound = true;
                } else {
                    if (nums2[j] > nums1[i])  {
                        result[i] = nums2[j];
                        greaterFound = true;
                        break;
                    }
                }
            }
            if (!greaterFound)
                result[i] = -1;
        }
        return result;
    }

    //copied best solution
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {

        int[] ind = new int[10000];
        int[] max = new int[nums2.length];
        int pointer = 0;

        for (int i = 0; i < nums1.length; i++) {
            ind[nums1[i]] = i + 1;
        }

        int[] res = new int[nums1.length];
        for (int i = nums2.length - 1; i >= 0; i--) {
            int num = nums2[i];
            while (pointer > 0 && max[pointer - 1] < num) {
                pointer--;
            }
            if (ind[num] > 0) {
                res[ind[num] - 1] = pointer == 0 ? -1 : max[pointer - 1];
            }
            max[pointer++] = num;
        }
        return res;
    }

    // yunis solution
    public int[] nextGreaterElement2(int[] nums1, int[] nums2) {
        Stack<Integer> stack = new Stack<>();
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        stack.push(-1);
        boolean found = false;
        for (int i = nums2.length-1; i >=0; i--) {
            found = false;
            if (nums2[i] >= stack.peek()) {
                while (!stack.isEmpty()) {
                    if (nums2[i] < stack.peek()) {
                        found = true;
                        hashMap.put(nums2[i], stack.peek());
                        break;
                    }
                    stack.pop();
                }
                if (!found) {
                    hashMap.put(nums2[i], -1);
                }
                stack.push(nums2[i]);
            } else {
                hashMap.put(nums2[i], stack.peek());
                stack.push(nums2[i]);
            }
        }

        for (int i = 0;i<nums1.length;i++) {
            nums1[i] = hashMap.get(nums1[i]);
        }
        return nums1;
    }
}

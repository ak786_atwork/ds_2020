package problems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NodesAtKDistance {

    public static void main(String[] args) {

    }

    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        List<Integer> list = new ArrayList<>();
        HashMap<Integer, TreeNode> map = new HashMap<>();

        int level = findAllParents(root, target, 0, map) ;
        TreeNode tempNode;
        for (int i = 0; i <= level; i++) {
            tempNode = map.get(level);
            addNodes(tempNode, map.get(level-1),K-level,list);
        }
        return list;
    }

    public Integer findAllParents(TreeNode currentNode, TreeNode node, int currentLevel, HashMap<Integer, TreeNode> map) {
        if (currentNode == node) {
            map.put(0,currentNode);
            return currentLevel + 1;
        }
        Integer leftResponse = null;
        Integer rightResponse = null;

        if (currentNode.left != null) {
            leftResponse = findAllParents(currentNode.left, node, currentLevel + 1, map);
            if (leftResponse != null) {
                map.put(leftResponse - currentLevel, currentNode.left);
                return leftResponse;
            }
        }
        if (currentNode.right != null) {
            rightResponse = findAllParents(currentNode.right, node, currentLevel + 1, map);
            if (rightResponse != null) {
                map.put(rightResponse - currentLevel, currentNode.right);
                return rightResponse;
            }
        }
        return null;
    }

    public void addNodes(TreeNode currentNode, TreeNode omitNode, int distance, List<Integer> list) {
        if (distance == 0) {
            list.add(currentNode.val);
        } else if (distance > 0) {
            if (currentNode.left != null) {
                if (omitNode != currentNode.left) {
                    addNodes(currentNode.left, null, distance - 1, list);
                }
            }
            if (currentNode.right != null) {
                if (omitNode != currentNode.right) {
                    addNodes(currentNode.right, null, distance - 1, list);
                }
            }
        }
    }


}


class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}

package problems;

public class PalindromeLinkedList {

    //submitted and working on leetcode
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
//        head.next.next = new problems.ListNode(1);

        printNodes(head);

        System.out.println((new PalindromeLinkedList()).isPalindrome(head));
    }

    private static void printNodes(ListNode head) {
        while (head != null) {
            System.out.print(head.val+" -> ");
            head = head.next;
        }
        System.out.println();
    }

    public boolean isPalindrome(ListNode head) {
        if (head == null || head.next == null)
            return true;

        ListNode[] nodes = findMid(head);
        ListNode leftListHead = nodes[0];
        ListNode rightListHead = nodes[1];
        printNodes(leftListHead);
        printNodes(rightListHead);

        while (rightListHead != null && leftListHead != null) {
            if (leftListHead.val != rightListHead.val)
                return false;
            rightListHead =rightListHead.next;
            leftListHead = leftListHead.next;
        }
        return true;
    }

    public ListNode[] findMid(ListNode head) {
        ListNode sptr = head;
        ListNode fptr = head;
        ListNode pre = null;
        ListNode temp = null;
        int count = 0;
        while (fptr != null && fptr.next != null) {
            count++;
            fptr = fptr.next.next;

            temp = sptr.next;
            sptr.next = pre;
            pre = sptr;
            sptr = temp;
        }
        if (fptr == null) {
            return new ListNode[]{pre,sptr};
        } else {
            return new ListNode[] {pre, sptr.next};
        }
    }
}

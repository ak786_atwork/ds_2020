package problems;

import java.util.ArrayList;
import java.util.List;

public class PowerSet {

    public static void main(String[] args) {
        int[] nums = {1,2,3};
        System.out.println((new PowerSetSolution()).subsets(nums).toString());
    }

    public List<List<Integer>> subsets(int[] nums) {
        int pairLenth = 0;
        List<List<Integer>> lists = new ArrayList<>();

        while (true) {
            if (++pairLenth > nums.length)
                break;

            for (int i = 1; i + pairLenth <= nums.length; i++) {
                for (int j = i+1; j + pairLenth -1 <= nums.length; j++) {
//                System.out.println("i = "+i +" pair length = "+pairLenth);
                    lists.add(addList(nums,i, j, pairLenth));

                }
            }
        }
        lists.add(new ArrayList<>());
        return lists;
    }

    private List<Integer> addList(int[] nums, int start, int takeFrom, int pairLenth) {
        List<Integer> list = new ArrayList<>();
        list.add(nums[start]);
        pairLenth--;
        while (pairLenth-- > 0) {
            list.add(nums[takeFrom++]);
        }

//        System.out.println(list.toString());
        return list;
    }

    private List<Integer> addList(int[] nums, int startFrom, int pairLenth) {
        List<Integer> list = new ArrayList<>();
        while (pairLenth-- > 0) {
            list.add(nums[startFrom++]);
        }

//        System.out.println(list.toString());
        return list;
    }

}

//submitted to leetcode
class PowerSetSolution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> lists = new ArrayList<>();
        List<Integer> tempList = new ArrayList<>();
        addAllCombinations(nums,tempList, 0, lists);
        System.out.println(lists.size());
        return lists;
    }

    private void addAllCombinations(int[] nums, List<Integer> tempList, int currentIndex, List<List<Integer>> resultList) {
        if (currentIndex > nums.length)
            return;
        //add this to list
        resultList.add(tempList);
        List<Integer> newList = null;
        for (int i = currentIndex;i<nums.length;i++) {
            newList = new ArrayList<>(tempList);
            newList.add(nums[i]);
            addAllCombinations(nums,newList,i+1,resultList);
        }
    }

    //from leetcode
    public List<List<Integer>> subsets1(int[] nums) {
        List<List<Integer>> output = new ArrayList();
        output.add(new ArrayList<Integer>());

        for (int num : nums) {
            List<List<Integer>> newSubsets = new ArrayList();
            for (List<Integer> curr : output) {
                newSubsets.add(new ArrayList<Integer>(curr){{add(num);}});
            }
            for (List<Integer> curr : newSubsets) {
                output.add(curr);
            }
        }
        return output;
    }

    //from leetcode
    public List<List<Integer>> subsets2(int[] nums) {
        List<List<Integer>> output = new ArrayList();
        int n = nums.length;

        for (int i = (int)Math.pow(2, n); i < (int)Math.pow(2, n + 1); ++i) {
            // generate bitmask, from 0..00 to 1..11
            String bitmask = Integer.toBinaryString(i).substring(1);

            // append subset corresponding to that bitmask
            List<Integer> curr = new ArrayList();
            for (int j = 0; j < n; ++j) {
                if (bitmask.charAt(j) == '1') curr.add(nums[j]);
            }
            output.add(curr);
        }
        return output;
    }
}

//from leetcode
class PSSolution {
    List<List<Integer>> output = new ArrayList();
    int n, k;

    public void backtrack(int first, ArrayList<Integer> curr, int[] nums) {
        // if the combination is done
        if (curr.size() == k)
            output.add(new ArrayList(curr));

        for (int i = first; i < n; ++i) {
            // add i into the current combination
            curr.add(nums[i]);
            // use next integers to complete the combination
            backtrack(i + 1, curr, nums);
            // backtrack
            curr.remove(curr.size() - 1);
        }
    }

    public List<List<Integer>> subsets(int[] nums) {
        n = nums.length;
        for (k = 0; k < n + 1; ++k) {
            backtrack(0, new ArrayList<Integer>(), nums);
        }
        return output;
    }
}

package problems;

public class ReverseLinkedList {

    //both correct submitted  to leetcode
    public ListNode reverseList(ListNode head) {
//        return reverseListIterative(head);

        if (head != null && head.next != null)
            return reverseLinkedListRecursive(head, null);
        return head;
    }

    public ListNode reverseLinkedListRecursive(ListNode current, ListNode pre) {
        if (current.next != null) {
            ListNode temp  = current.next;
            current.next = pre;
            return reverseLinkedListRecursive(temp, current);
        } else {
            current.next = pre;
            return current;
        }
    }

    public ListNode reverseListIterative(ListNode head) {
        if (head == null || head.next == null)
            return head;

        ListNode pre, current, temp;
        pre  = null;
        current = head;

        while (current.next != null) {
            temp = current.next;
            current.next = pre;
            pre = current;
            current = temp;
        }
        current.next = pre;
        return current;
    }

}



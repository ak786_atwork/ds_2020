package problems;

public class ReverseWordsInString {

    public static void main(String[] args) {
        String s = "    the sky is blue  ";
        char[] c = s.toCharArray();
        (new ReverseWordsInString()).reverseWords(c);
        System.out.println(c);
    }

    public void reverseWords(char[] s) {
        int wordStartIndex;
        int wordEndIndex;
        for (int i=0;i<s.length;i++) {
            if (s[i] != ' ') {
                wordStartIndex = i;
                while (i<s.length && s[i] != ' ') {
                    i++;
                }
                wordEndIndex = --i;
                reverse(s,wordStartIndex,wordEndIndex);
            }
        }
        reverse(s,0,s.length-1);
    }

    private void reverse(char[] s, int wordStartIndex, int wordEndIndex) {
        char temp;
        while (wordStartIndex < wordEndIndex) {
            temp = s[wordStartIndex];
            s[wordStartIndex] = s[wordEndIndex];
            s[wordEndIndex] = temp;
            wordStartIndex++;
            wordEndIndex--;
        }
    }
}

package problems;

//submitted on leetcode
public class RotateImage {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}};

        int[][] matrix2 = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        (new RotateImage()).rotate(matrix2);
    }

    public void rotate(int[][] matrix) {
        int row = 0;
        int col = 0;
        int temp = 0;
        int temp2 = 0;

        //loop for meshes
        for (int i = 0, j = matrix.length - 1; i < j; i++, j--) {
            //loop for elements to be rotated
            for (int k = i; k < j; k++) {
                col = k;
                temp = matrix[i][col];
                //loop for completing one round 1.e. 4
                for (int p = 0; p < 4; p++) {
                    row = col;
                    switch (p) {
                        case 0:
                            col = j;
                            break;
                        case 1:
                            col = j + i - k;
                            break;
                        case 2:
                            col = i;
                            break;
                        case 3:
                            col = k;
                            break;
                    }
                    temp2 = matrix[row][col];
                    matrix[row][col] = temp;
                    temp = temp2;
                }
            }
        }
    }

    private void printMatrix(int[][] matrix) {
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++)
                System.out.print(matrix[i][j] + " ");
            System.out.println();
        }
    }
}

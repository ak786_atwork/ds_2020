package problems;

public class SeachIn2DSortedMatrix {

    public static void main(String[] args) {
//        System.out.println((new problems.SeachIn2DSortedMatrix()).binarySearch());
    }

    //submitted on leetcode
    public boolean binarySearch(int[][] matrix, int start, int end, int number) {
        if (start > end || matrix.length == 0)
            return false;
        int mid = (start + end)/2;
        int row = mid/matrix[0].length;
        int col = mid % matrix[0].length;

        if (matrix[row][col] == number)
            return true;
        else if (number < matrix[row][col])
            return binarySearch(matrix, start, mid-1,number);
        else return binarySearch(matrix, mid+1,end,number);
    }
}

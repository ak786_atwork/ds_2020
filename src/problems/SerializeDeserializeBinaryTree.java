package problems;

import test.A;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//submitted but memory limit exceeded
public class SerializeDeserializeBinaryTree {

    public static void main(String[] args) {
        SDTreeNode root  = new SDTreeNode(2);
        root.left = new SDTreeNode(1);
        root.right = new SDTreeNode(3);

        PreOrderSolution serializeDeserializeBinaryTree = new PreOrderSolution();
        String s = serializeDeserializeBinaryTree.serialize(null);
        System.out.println(s);
        serializeDeserializeBinaryTree.deserialize(s);
//        System.out.println();
    }


    // Encodes a tree to a single string.
    public  String serialize(SDTreeNode root) {

        //bfs
        Queue<SDTreeNode> queue = new LinkedList<>();
        queue.offer(root);

        ArrayList<Integer> list = new ArrayList<>();
        int nodeAtLevel = 0;
        int nullNodes = 0;
        boolean exit = false;
        SDTreeNode temp = null;
        while (!queue.isEmpty()) {
            nullNodes = 0;
            nodeAtLevel = queue.size();
            for (int i = 0; i< nodeAtLevel;i++) {
                temp = queue.poll();
                if (temp == null) {
                    nullNodes++;
                    list.add(null);
                    queue.add(null);
                    queue.add(null);
                } else {
                    list.add(temp.val);
                    queue.add(temp.left);
                    queue.add(temp.right);
                }
                if (nodeAtLevel == nullNodes) {
                    exit = true;
                    break;
                }
            }
            if (exit)
                break;
        }
//        String[] s = list.toString().split(",");
//        for (String w : s)
//            System.out.println(w);
        String res = list.toString();
        return res.substring(1,res.length()-1);
        /*nodeAtLevel = queue.size();
            for (int i = 0;i<nodeAtLevel;i++) {
                temp = queue.poll();
                if (temp != null){}
            }
        * */
     /*   ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("c");
        list.add("b");
        System.out.println(list);

        String[] s = list.toString().substring(1,list.toString().length()).split(",");
        System.out.println(s);*/
//        return null;
    }

    // Decodes your encoded data to tree.
    public SDTreeNode deserialize(String data) {
        String[] s = data.split(", ");
        Integer[] nodes = new Integer[s.length];

        Integer temp = null;
        for (int i = 0; i < nodes.length;i++) {
            temp = null;
            if (!s[i].equals("null")) {
                temp = Integer.parseInt(s[i]);
            }
            nodes[i] = temp;
        }

        return buildTree(0,nodes);




//        return null;
    }

    private SDTreeNode buildTree(int nodeIndex, Integer[] nodes) {
        if (nodes[nodeIndex] == null)
            return null;

        SDTreeNode node = new SDTreeNode(nodes[nodeIndex]);
        node.left = buildTree(2*nodeIndex + 1, nodes);
        node.right = buildTree(2*nodeIndex + 2, nodes);
        return node;
    }
}

 class SDTreeNode {
      int val;
      SDTreeNode left;
      SDTreeNode right;
      SDTreeNode(int x) { val = x; }
  }

  //mle
class SDS {
    // Encodes a tree to a single string.
    public  String serialize(SDTreeNode  root) {

        //bfs
        Queue<SDTreeNode> queue = new LinkedList<>();
        queue.offer(root);

        StringBuilder list = new StringBuilder();
        int nodeAtLevel = 0;
        int nullNodes = 0;
        boolean exit = false;
        SDTreeNode temp = null;
        int endLen = list.length();
        while (!queue.isEmpty()) {
            nullNodes = 0;
            nodeAtLevel = queue.size();
            endLen = list.length();
            for (int i = 0; i< nodeAtLevel;i++) {
                temp = queue.poll();
                if (temp == null) {
                    nullNodes++;
                    list.append("null,");
                    queue.add(null);
                    queue.add(null);
                } else {
                    list.append(temp.val).append(",");
                    queue.add(temp.left);
                    queue.add(temp.right);
                }
                if (nodeAtLevel == nullNodes) {
                    exit = true;
                    break;
                }
            }
            if (exit)
                break;
        }
//        String[] s = list.toString().split(",");
//        for (String w : s)
//            System.out.println(w);
        return list.substring(0, endLen-1);
        /*nodeAtLevel = queue.size();
            for (int i = 0;i<nodeAtLevel;i++) {
                temp = queue.poll();
                if (temp != null){}
            }
        * */
     /*   ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("c");
        list.add("b");
        System.out.println(list);

        String[] s = list.toString().substring(1,list.toString().length()).split(",");
        System.out.println(s);*/
//        return null;
    }

    // Decodes your encoded data to tree.
    public SDTreeNode deserialize(String data) {
        String[] s = data.split(",");
        Integer[] nodes = new Integer[s.length];

        Integer temp = null;
        for (int i = 0; i < nodes.length;i++) {
            temp = null;
            if (!s[i].equals("null")) {
                temp = Integer.parseInt(s[i]);
            }
            nodes[i] = temp;
        }

        return buildTree(0,nodes);




//        return null;
    }

    private SDTreeNode buildTree(int nodeIndex, Integer[] nodes) {
        if (nodeIndex >= nodes.length || nodes[nodeIndex] == null)
            return null;

        SDTreeNode node = new SDTreeNode(nodes[nodeIndex]);
        node.left = buildTree(2*nodeIndex + 1, nodes);
        node.right = buildTree(2*nodeIndex + 2, nodes);
        return node;
    }
}

class PreOrderSolution {
    static int nodeIndex;
    // Encodes a tree to a single string.
    public  String serialize(SDTreeNode  root) {
        if (root == null)
            return "";
        StringBuilder sb = new StringBuilder();
        serializeHelper(sb,root);
        return sb.substring(0,sb.length()-1);
    }

    private void serializeHelper(StringBuilder sb, SDTreeNode root) {
        if (root == null) {
            sb.append("null,");
            return;
        }
        sb.append(root.val).append(",");

        serializeHelper(sb,root.left);
        serializeHelper(sb,root.right);
    }


    // Decodes your encoded data to tree.
    public SDTreeNode deserialize(String data) {
        if (data.isEmpty())
            return null;
        String[] s = data.split(",");
        Integer[] nodes = new Integer[s.length];

        Integer temp = null;
        for (int i = 0; i < nodes.length;i++) {
            temp = null;
            if (!s[i].equals("null")) {
                temp = Integer.parseInt(s[i]);
            }
            nodes[i] = temp;
        }
        nodeIndex = -1;

        return buildTree(nodes);
    }

    private SDTreeNode buildTree(Integer[] nodes) {
        nodeIndex++;
        if (nodeIndex >= nodes.length || nodes[nodeIndex] == null)
            return null;

        SDTreeNode node = new SDTreeNode(nodes[nodeIndex]);
        node.left = buildTree(nodes);
        node.right = buildTree(nodes);
        return node;
    }
}

//from leetcode
 class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if (root == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        preorder(root, sb);
        return sb.toString();
    }

    private void preorder(TreeNode root, StringBuilder sb) {
        if (root == null) {
            return;
        }
        sb.append((char)root.val);
        preorder(root.left, sb);
        preorder(root.right, sb);
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data == null || data.length() == 0) {
            return null;
        }
        char[] chs = data.toCharArray();
        return helper(chs, new int[1], Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private TreeNode helper(char[] chs, int[] index, int low, int high) {
        if (index[0] == chs.length) {
            return null;
        }
        int temp = (int)chs[index[0]];
        if (temp >= low && temp <= high) {
            index[0]++;
            TreeNode root = new TreeNode(temp);
            root.left = helper(chs, index, low, temp);
            root.right = helper(chs, index, temp, high);
            return root;
        }
        return null;
    }
}
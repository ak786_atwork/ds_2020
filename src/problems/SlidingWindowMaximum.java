package problems;

import javafx.collections.transformation.SortedList;

import java.util.*;

public class SlidingWindowMaximum {

    public static void main(String[] args) {
        int[] nums = {1,3,-1,-3,5,3,6,7};
//        int[] nums = {-7,-8,7,5,7,1,6,0};
        int k = 3;
//        printArray((new SlidingWindowMaximum()).maxSlidingWindow(nums, k));
        printArray((new SWM()).maxSlidingWindow(nums, k));
    }
    private static void printArray(int[] nums) {
        System.out.println();
        for (int i=0;i<nums.length;i++)
            System.out.print(nums[i] +" ");
    }

    //submitted
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums.length == 0)
            return new int[0];
        int[] res = new int[nums.length - k +1];
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();

        for (int i = 0;i<k;i++) {
            if (sortedMap.containsKey(nums[i])) {
                sortedMap.put(nums[i], sortedMap.get(nums[i]) + 1);
            } else {
                sortedMap.put(nums[i], 1);
            }
        }
        int j = 0;
        res[j++] = sortedMap.lastKey();

        for (int i = k;i<nums.length;i++) {
            //remove or decrement count
            if (sortedMap.containsKey(nums[i-k]) && sortedMap.get(nums[i-k]) > 1) {
                sortedMap.put(nums[i-k], sortedMap.get(nums[i-k]) - 1);
            } else {
                sortedMap.remove(nums[i - k]);
            }
            if (sortedMap.containsKey(nums[i])) {
                sortedMap.put(nums[i], sortedMap.get(nums[i]) + 1);
            } else {
                sortedMap.put(nums[i], 1);
            }
            res[j++] = sortedMap.lastKey();
        }
        return res;
    }
}

class SWM {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (k==1)
            return nums;
        if (nums.length == 0)
            return new int[0];

        int[] res = new int[nums.length - k +1];
        Deque<Integer> deque = new ArrayDeque<>();
        deque.addFirst(0);

        for (int i = 1;i<nums.length;i++) {
            if (!deque.isEmpty()) {
                //check outdated
                if (deque.peekFirst() + k - 1 < i) {
                    //outdated
                    deque.pollFirst();
                }
                if (nums[i] > nums[deque.peekLast()]) {
                    //pop elements until you find equal
                    while (!deque.isEmpty() && nums[i] > nums[deque.peekLast()]) {
                        deque.pollLast();
                    }
                }
                deque.addLast(i);
                if (i-k+1 >= 0) {
                    //update res
                    res[i-k+1] = nums[deque.peekFirst()];
                }
            }
        }
        return res;
    }
}

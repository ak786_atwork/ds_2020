package problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StringPermutation {
    int permutations = 0;

    public static void main(String[] args) {
        String s = "abcd";
//        problems.StringPermutation test = new problems.StringPermutation();
//        test.permute(s.toCharArray(),0);
//        System.out.println(test.permutations);
//        int[] nums = {1,2,3};
//        System.out.println((new problems.StringPermutation()).permute(nums).toString());
    }

    public void permute(char[] s, int i) {
//        System.out.println("called");
        if (i >= s.length-1) {
            permutations++;
            System.out.println(s);
            return;
        }
//        permute(s,i+1);
        for (int j = i; j < s.length; j++) {
            swap(s,j,i);
            permute(s, i+1);
            swap(s,j,i);
        }
    }


    private void swap(char[] s, int j, int i) {
        char temp = s[j];
        s[j] = s[i];
        s[i] = temp;
    }
}

//leetcode submitted passed all
class Permutation {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> lists = new ArrayList<>();
        findPermutations(nums,0,lists);
        return lists;
    }

    public void findPermutations(int[] s, int i, List<List<Integer>> lists) {
        if (i >= s.length-1) {
            addArrayToList(s, lists);
            return;
        }
        for (int j = i; j < s.length; j++) {
            swap(s,j,i);
            findPermutations(s, i+1,lists);
            swap(s,j,i);
        }
    }

    private void addArrayToList(int[] s, List<List<Integer>> lists) {
        List<Integer> list = new ArrayList<>();
        for (int i =0;i<s.length;i++) {
            list.add(s[i]);
        }
        lists.add(list);
    }

    private void swap(int[] s, int j, int i) {
        int temp = s[j];
        s[j] = s[i];
        s[i] = temp;
    }
}

package problems;

import java.util.*;

public class ThreeSum {

    public static void main(String[] args) {
        int[] nums = {-2,0,0,2,2};
        (new ThreeSum()).threeSum3(nums);
    }

    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> lists = new ArrayList<>();
        Set<String> set = new HashSet<>();
        int number = 0;
        int target = 0;
        int start = 0;
        int end = 0;
        Arrays.sort(nums);
//        printArray(nums);
        for (int i = 0; i < nums.length - 2; i++) {
            target = number - nums[i];
            System.out.println("--");
            start = i + 1;
            end = nums.length - 1;
            while (start < end) {
                if (nums[start] + nums[end] == target) {
                    if (!set.contains(nums[i] + "" + nums[start] + "" + nums[end])) {
                        lists.add(new ArrayList<Integer>(Arrays.asList(nums[i], nums[start], nums[end])));
                        set.add(nums[i] + "" + nums[start] + "" + nums[end]);
                    }
//                    break;
                } else if (nums[start] + nums[end] > target) {
                    end--;
                } else {
                    start++;
                }
            }

        }
        System.out.println(lists.toString());
        return lists;
    }


    //submitted to leetcode working
    public List<List<Integer>> threeSum1(int[] nums) {
        List<List<Integer>> lists = new ArrayList<>();
        int number = 0;
        int target = 0;
        int start = 0;
        int end = 0;
        int last = 0;
        int subLast = 0;
        Arrays.sort(nums);
//        printArray(nums);
        for (int i = 0; i < nums.length - 2; i++) {

            if (i != 0) {
                if (last == nums[i]) {
                    last = nums[i];
                    continue;
                }
            }
            last = nums[i];

            target = number - nums[i];
            start = i + 1;
            end = nums.length - 1;
            while (start < end) {
                if (nums[start] + nums[end] == target) {
                    if (start != i+1) {
                        if (nums[start] == nums[start-1]) {
                            start++;
                            continue;
                        }
                    }
                    lists.add(new ArrayList<Integer>(Arrays.asList(nums[i], nums[start], nums[end])));
                    start++;
                    end--;
                } else if (nums[start] + nums[end] > target) {
                    end--;
                } else {
                    start++;
                }
            }

        }
        System.out.println(lists.toString());
        return lists;
    }

    private void printArray(int[] nums) {
        System.out.println();
        for (int num : nums) {
            System.out.print(num + " ");
        }
    }

    //tle but working
    public List<List<Integer>> threeSum3(int[] nums) {
        int last =0;
        List<List<Integer>> lists = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 0;i<nums.length-2;i++) {
            if (i!=0) {
                if (last == nums[i]) {
                    continue;
                }
            }
            last = nums[i];
            for (int j = i+1; j < nums.length-1;j++) {
                if (j != i + 1) {
                    if (nums[j] == nums[j-1]) {
                        continue;
                    }
                }
                for (int k = j+1; k < nums.length;k++) {
                    if (nums[i] + nums[j] + nums[k] == 0) {
                        if (k != j + 1) {
                            if (nums[k] == nums[k-1]) {
                                continue;
                            }
                        }
                        lists.add(new ArrayList<Integer>(Arrays.asList(nums[i], nums[j], nums[k])));
                    }
                }
            }
        }

        System.out.println(lists.toString());
        return lists;
    }
}

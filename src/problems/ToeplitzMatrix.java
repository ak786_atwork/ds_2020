package problems;


public class ToeplitzMatrix {

    static boolean isToeplitz(int[][] a) {
        // your code goes here
        int k = 0;
        int temp = 0;
        for (int i = 0; i < a[0].length; i++) {
            k = 0;
            temp = a[k][i];
            for (int j = i; j < a.length; j++) {
                if (temp != a[k++][j])
                    return false;
            }
        }

        for (int i = 1; i < a.length; i++) {
            k = 0;
            temp = a[i][k];
            for (int j = i; j < a.length; j++) {
                if (temp != a[j][k++])
                    return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int[][] a = {
                {1, 2, 3, 4},
                {5, 1, 2, 3},
                {6, 5, 1, 2}};

        System.out.println(isToeplitz(a));
    }
}

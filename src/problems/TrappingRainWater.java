package problems;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.Stack;

public class TrappingRainWater {

    public static void main(String[] args) {
//        int[] height = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
        int[] height = {3,2,1,1,3};
//        int[] height = {6,4,2,0,3,2,0,3,1,4,5,3,2,7,5,3,0,1,2,1,3,4,6,8,1,3};
//        System.out.println((new TrappingRainWater()).trap(height));
        System.out.println((new TPS()).trap(height));
    }

    //tle
    public int trap(int[] height) {
        int maxHeight = getMax(height);
        boolean firstBlockFound = false;
        boolean endBlockNeeded = false;
        int totalTrappedWater = 0;
        int currentPossibleTrappedWater = 0;

        while (maxHeight-- > 0) {
            for (int i = 0; i < height.length; i++) {
                if (!firstBlockFound && height[i] <= 0) {
                } else {
                    firstBlockFound = true;
                    if (height[i] <= 0) {
                        currentPossibleTrappedWater++;
                        endBlockNeeded = true;
                    } else {
                        if (endBlockNeeded) {
                            totalTrappedWater += currentPossibleTrappedWater;
                            currentPossibleTrappedWater = 0;
                            endBlockNeeded = false;
                        }
                    }
                }
                height[i] = height[i] - 1;
            }
            firstBlockFound = false;
            currentPossibleTrappedWater = 0;
            endBlockNeeded = false;
        }
        return totalTrappedWater;
    }

    private int getMax(int[] height) {
        int max = height[0];
        for (int i = 1; i < height.length; i++) {
            if (max < height[i])
                max = height[i];
        }
        return max;
    }
}

//wrong answer --- but we can do using stacks
class TPS {
    public int trap(int[] height) {
        if (height.length < 3)
            return 0;
        int i = 0;
        //skip zeros
        for (; i < height.length; i++) {
            if (height[i] != 0)
                break;
        }
        if (i + 2 >= height.length)
            return 0;

        int totalWater = 0;
        int processedArea = 0;
        int width = 0;
        Stack<Integer> stack = new Stack<>();
        stack.push(height[i++]);
        while (!stack.isEmpty() && i < height.length) {
            if (height[i] <= stack.peek()) {
                if (stack.size() == 1 && height[i] == stack.peek()) {
                    i++;
                    continue;
                }
                stack.push(height[i]);
            } else {
                if (stack.size() == 1) {
                    stack.pop();
                    stack.push(height[i]);
                    i++;
                    width = 0;
                    processedArea = 0;
                    continue;
                }
                //process
                int rightBlock = height[i];
                int totalBlock = 0;
                while (stack.peek() < rightBlock && stack.size() != 1) {
                    totalBlock += stack.pop();
                    width++;
                }
                int leftBlock = stack.pop();
                totalWater += width * Math.min(rightBlock, leftBlock) - totalBlock - processedArea;
                processedArea = ++width * Math.min(leftBlock, rightBlock);
                stack.push(Math.max(leftBlock,rightBlock));

                if (rightBlock >= leftBlock && stack.size() == 1) {
                    width = 0;
                    processedArea = 0;
                }
            }
            i++;
        }
        return totalWater;
    }
}

//best solution from leetcode
class TPS1 {
    public int trap(int[] height) {
        if (height.length < 3)
            return 0;
        int left = 0;
        int right = height.length-1;
        int totalWater = 0;
        int leftMax = height[0];
        int rightMax = height[right];

        while (left <= right) {
            if (height[left] <= height[right]) {
                leftMax = Math.max(leftMax, height[left]);
                totalWater += Math.min(leftMax, height[right]) - height[left];
                left++;
            } else {
                rightMax = Math.max(rightMax, height[right]);
                totalWater += Math.min(height[left], rightMax) - height[right];
                right--;
            }
            System.out.println("left = "+left+" right = "+right+" total water = "+totalWater);
        }
        return totalWater;
    }
}
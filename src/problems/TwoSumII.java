package problems;

import java.util.HashMap;

public class TwoSumII {
    public int[] twoSum(int[] numbers, int target) {
       int i = 0;
       int j = numbers.length-1;
       while (i < j) {
           if (numbers[i] + numbers[j] < target) {
               i++;
           } else if (numbers[i] + numbers[j] > target) {
               j--;
           } else {
               return new int[]{i+1,j+1};
           }
       }
       return null;
    }

    public int[] twoSum1(int[] numbers, int target) {
        HashMap<Integer,Integer> map = new HashMap<>();
        for (int i = 0;i<numbers.length;i++) {
            if (map.containsKey(target - numbers[i])) {
                return new int[] {map.get(target-numbers[i])+1, i+1};
            }
            map.put(numbers[i], i);
        }
        return null;
    }

}

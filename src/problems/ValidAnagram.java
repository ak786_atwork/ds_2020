package problems;

import java.util.HashMap;
import java.util.Map;

public class ValidAnagram {

    public static void main(String[] args) {
        String s = "a";
        String t = "ab";
        System.out.println((new ValidAnagram()).isAnagram1(s, t));
    }

    public boolean isAnagram(String s, String t) {
        int[] array = new int[26];
        int index = 0;
        for (int i = 0; i < s.length(); i++) {
            index = s.charAt(i) - 97;
            array[index] = array[index] + 1;
        }
        for (int i = 0; i < t.length(); i++) {
            index = t.charAt(i) - 97;
            array[index] = array[index] - 1;
        }

        for (int i = 0; i < 26; i++) {
            if (array[i] != 0)
                return false;
        }
        return true;
    }

    public boolean isAnagram1(String s, String t) {

        if (s.length() != t.length())
            return false;

        Map<Character, Integer> map = new HashMap<>();

        for (int i = 0; i<s.length();i++) {
            if (s.charAt(i) != t.charAt(i)) {
                if (map.containsKey(s.charAt(i))) {
                    map.put(s.charAt(i), map.get(s.charAt(i)) + 1);
                } else {
                    map.put(s.charAt(i),1);
                }

                if (map.containsKey(t.charAt(i))) {
                    map.put(t.charAt(i), map.get(t.charAt(i)) - 1);
                } else {
                    map.put(t.charAt(i),-1);
                }
            }
        }

        for (Map.Entry<Character, Integer> item : map.entrySet()) {
            if (item.getValue() != 0)
                return false;
        }

        return true;
    }

}

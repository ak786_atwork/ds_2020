package problems;

public class ValidateBST {

    private static int lastNumber = Integer.MIN_VALUE;

    public static void main(String[] args) {
        TreeNode root = new TreeNode(0);
        root.left = new TreeNode(-1);
//        root.left.right = new problems.TreeNode(12);
//        root.right = new problems.TreeNode(20);
        System.out.println((new ValidateBST()).isValidBST(root));
    }

    public boolean isValidBST(TreeNode root) {
        if (root == null)
            return true;

        return isBST1(root);
    }

    /*private boolean isBST(problems.TreeNode root) {
        if (root == null)
            return true;
        boolean isLeftBST = isBST(root.left);
        if (isLeftBST) {
            if (lastNumber < root.val)
                lastNumber = root.val;
            else {
                System.out.println("return from left bst at ---"+root.val);
                return false;
            }
        } else {
            System.out.println("return from left bst at "+root.val);
            return false;
        }

        System.out.println(root.val +" last number = "+lastNumber);

//        boolean isRightBST = isBST(root.right);
        if (isBST(root.right)) {
            if (lastNumber < root.val)
                lastNumber = root.val;
            else {
                System.out.println("return from right bst at --- "+root.val);
                return false;
            }
        } else {
            System.out.println("return from right bst at "+root.val);
            return false;
        }

        return true;
    }*/

    private boolean isBST1(TreeNode root) {
        if (root.left != null) {
            if (!isBST1(root.left))
                return false;
        }
        if (lastNumber < root.val)
            lastNumber = root.val;
        else {
            return false;
        }

        if (root.right != null){
            if (!isBST1(root.right))
                return false;
        }

        return true;
    }

}

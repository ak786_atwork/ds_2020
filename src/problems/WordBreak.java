package problems;

import java.util.*;

public class WordBreak {
    TrieNode root;

    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        //"cats", "dog", "sand", "and", "cat"
        //"cars"
        //["car","ca","rs"]
        List<String> list = new ArrayList<>();
//        list.add("cats");
//        list.add("dog");
//        list.add("sand");
//        list.add("and");
//        list.add("cat");
//        list.add("car");
//        list.add("rs");
//        list.add("ca");
//        String s = "aaaaaaaaaaaaaaaaaaaaaaaaaaab";
        String s = "catisdog";
//        String s = "aaaaaaa";

        //String[] array = {"a","aa","aaa","aaaa"};//,"aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"};
        String[] array = {"is","cat","do"};//,"aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"};

        list = getList(array);
//        String s = "cars";

        System.out.println("-----" + (new WBSolution()).wordBreak(s, list));
        System.out.println(System.currentTimeMillis() - time);
    }

    private static List<String> getList(String[] array) {
        List<String> list = new ArrayList<>();
        for (String s : array) {
            list.add(s);
        }
        return list;
    }

    public boolean wordBreak(String s, List<String> wordDict) {
        if (wordDict.size() == 0)
            return false;
        else if (wordDict.size() == 1) {
            if (wordDict.get(0).equals(s))
                return true;
            else return false;
        }
        Trie trie = new Trie();
        for (String str : wordDict) {
            trie.insert(str);
        }
        root = trie.root;
//        trie.printAllWords(root,"");
        return searchSegment(s, 0);

    }

    //tle submitted on leetcode
    private boolean searchSegment(String s, int index) {
        TrieNode node = root;
        for (int i = index; i < s.length(); i++) {
            if (node.endOfWord) {
                //a word has completed at last char, from this char, search for other segment from beginning
                if (searchSegment(s, i)) {
                    return true;
                }
            }
            if (node.map.containsKey(s.charAt(i))) {
                node = node.map.get(s.charAt(i));
            } else if (!node.endOfWord)
                return false;
            else {
                i--;
                node = root;
            }
        }
        if (node.endOfWord)
            return true;

        return false;
    }
}

class Trie {
    TrieNode root = new TrieNode();

    public void insert(String s) {
        TrieNode node = root;
        int i = 0;
        for (; i < s.length(); i++) {
            if (node.map.containsKey(s.charAt(i))) {
                node = node.map.get(s.charAt(i));
            } else {
                //insert all char from this index
                insertAll(node, s, i);
                break;
            }
        }
        if (i == s.length())
            node.endOfWord = true;
    }

    private void insertAll(TrieNode node, String s, int i) {
        int j = s.length() - 1;
        TrieNode lastNode = new TrieNode(true);
        while (j > i) {
            TrieNode newNode = new TrieNode();
            newNode.map.put(s.charAt(j), lastNode);
            lastNode = newNode;
            j--;
        }
        node.map.put(s.charAt(j), lastNode);
    }

    public void printAllWords(TrieNode node, String s) {
        if (node.endOfWord)
            System.out.println(s);
        System.out.println("map = " + node.map.toString() + " size = " + node.map.size() + "end = " + node.endOfWord);
        for (Map.Entry<Character, TrieNode> entry : node.map.entrySet()) {
            printAllWords(entry.getValue(), s + entry.getKey());
        }

    }
}

class TrieNode {
    HashMap<Character, TrieNode> map;
    boolean endOfWord;

    public TrieNode(HashMap<Character, TrieNode> map, boolean endOfWord) {
        this.map = map;
        this.endOfWord = endOfWord;
    }

    public TrieNode() {
        this.map = new HashMap<>();
    }

    public TrieNode(boolean endOfWord) {
        this.map = new HashMap<>();
        this.endOfWord = endOfWord;
    }
}

//working
class WordBreakSolution {
    public boolean wordBreak(String s1, List<String> wordDict) {
        // create a set out of given list for faster lookup
        Set<String> dict = new HashSet<String>();
        for(String s:wordDict){
            dict.add(s);
        }
        // an array for memoization
        //memo[i] = true if we can break s1[0:i] successfully ( i is exclusive)
        //memo[i] = false if we cannot break s1[0:i]
        boolean [] memo = new boolean[s1.length()+1];
        memo[0]=true;// base case, we can break s1[0:0]
        // check with s1 ending at 1 to s1.length()
        for(int end=1;end<=s1.length();end++){
            // check all the substrings from 0 to end
            for(int start=0;start<end;start++){
                if(dict.contains(s1.substring(start,end)) && memo[start]){
                    memo[end]=true;
                }
            }
        }
        return memo[s1.length()];

    }
}

//best solution 100% runtime
class WBSolution {
    public boolean wordBreak(String s, List<String> dict) {
        return bt(s, dict, new boolean[s.length()], 0);
    }

    //top down
    private boolean bt(String s, List<String> dict, boolean[] visited, int begin) {
        if (begin >= s.length()) return true;
        if (visited[begin]) return false;
        for (String word : dict) {
            if (s.startsWith(word, begin)) {
                if (bt(s, dict, visited, begin + word.length())) return true;
                visited[begin] = true;
            }
        }
        return false;
    }

    //submitted on leetcode
    private boolean bottomUpWB(String s, List<String> dict) {
        boolean[] dp = new boolean[s.length()+1];
        dp[0] = true;

        for (int i = 1; i<=s.length();i++) {
            for (int j = 0; j < i;j++) {
                if (dp[j] && dict.contains(s.substring(j,i))) {
                    dp[i] = true;
                    break;
                }
            }
        }

        return dp[s.length()];
    }
}

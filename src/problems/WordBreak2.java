package problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class WordBreak2 {

    public static void main(String[] args) {
        String s = "catsanddog";
        List<String> list = Arrays.asList("cat","cats","and","sand","dog");
        System.out.println((new WordBreak2()).wordBreak(s,list));
    }

    public List<String> wordBreak(String s, List<String> dict) {
//        List<String> list = new ArrayList<String>();
//        bt(s, dict, new boolean[s.length()], 0, list, "");
//        return list;
        return wb(s,dict,new HashMap<String, List<String>>());
    }


    private void bt(String s, List<String> dict, boolean[] visited, int begin, List<String> list, String currentString) {
        if (begin >= s.length()) {
            list.add(currentString.trim());
        }
        for (String word : dict) {
            if (s.startsWith(word, begin)) {
                bt(s, dict, visited, begin + word.length(), list, currentString + " " + word);
            }
        }
    }


    private List<String> wb(String s, List<String> dict, HashMap<String, List<String>> map) {
        if (s.isEmpty())
            return new ArrayList<String>();
        if (map.containsKey(s))
            return map.get(s);

        List<String> list = new ArrayList<>();

        for (String word : dict) {
            if (s.startsWith(word)) {
                if (s.length() == word.length())
                    list.add(word);
                else  {
                    List<String> temp = wb(s.substring(word.length()), dict, map);
                    for (String w : temp) {
                        list.add(word+" "+w);
                    }
                }
            }
        }
        System.out.println(s +" = "+ list);
        map.put(s,list);
        return list;
    }

}

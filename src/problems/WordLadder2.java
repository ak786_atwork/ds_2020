package problems;

import sun.plugin.javascript.navig.LinkArray;

import java.util.*;

//submitted on leetcode 1086ms
public class WordLadder2 {

    public static void main(String[] args) {
        /*
        * "red"
"tax"
["ted","tex","red","tax","tad","den","rex","pee"]
        * */
        String beginWord = "red";
        String endWord = "tax";
        List<String> list = Arrays.asList("ted","tex","red","tax","tad","den","rex","pee");
        System.out.println((new WLS3Solution()).findLadders(beginWord, endWord, list));
        //[["hot","dot","dog"],["hot","hog","dog"]]
    }

    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> resultList = new ArrayList<>();
        if (wordList.isEmpty()) {
            return resultList;
        }
        Set<String> set = new HashSet<>(wordList);
        set.remove(beginWord);
        if (!set.contains(endWord))
            return resultList;

        //build level map
        Map<Integer, List<String>> levelMap = buildMap(beginWord, endWord, new HashSet<String>(set));
        System.out.println(levelMap.toString());
        if (levelMap.isEmpty())
            return resultList;

        //process
        List<String> temp = new ArrayList<>();
        temp.add(beginWord);
        addLadders(beginWord, endWord, temp,1,levelMap,resultList);

        return resultList;
    }

    private void addLadders(String beginWord, String endWord, List<String> list, int level, Map<Integer, List<String>> levelMap, List<List<String>> resultList) {
        if (level == levelMap.size())
            return;
        if (endWord.equals(levelMap.get(level).get(0))) {
            if (!canReach(beginWord, endWord))
                return;
            List<String> copyList = new ArrayList<>(list);
            copyList.add(endWord);
            resultList.add(copyList);
            return;
        }

        Set<String> set = new HashSet<>(levelMap.get(level));

        //tle
       /* for (String s : levelMap.get(level)) {
            if (canReach(beginWord,s)) {
                list.add(s);
                addLadders(s,endWord,list,level+1,levelMap,resultList);
                list.remove(s);
            }
        }*/


        for (int j = 0; j < beginWord.length(); j++) {
            char[] newWord = beginWord.toCharArray();
            for (char c = 'a'; c <= 'z'; c++) {
                newWord[j] = c;
                String s = new String(newWord);
                if (!beginWord.equals(s) && set.contains(s)) {
                    list.add(s);
                    set.remove(s);
                    addLadders(s,endWord,list,level+1,levelMap,resultList);
                    list.remove(s);
                }
            }
        }

    }

    private boolean canReach(String beginWord, String endWord) {
        for (int j = 0; j < beginWord.length(); j++) {
            char[] newWord = beginWord.toCharArray();
            for (char c = 'a'; c <= 'z'; c++) {
                newWord[j] = c;
                String s = new String(newWord);
                if (!beginWord.equals(s) && s.equals(endWord)) {
                    return true;
                }
            }
        }
        return false;
    }


    private Map<Integer, List<String>> buildMap(String beginWord, String endWord, HashSet<String> set) {
        Map<Integer, List<String>> levelMap = new HashMap<>();
        levelMap.put(0, Arrays.asList(beginWord));

        int level = 0;
        List<String> list = null;
        boolean canReachEndWord = true;
        while (!set.isEmpty()) {
            if (!levelMap.containsKey(level)) {
                canReachEndWord = false;
                break;
            }
            list = levelMap.get(level);
            level++;
            for (String word : list) {
                for (int j = 0; j < word.length(); j++) {
                    char[] newWord = word.toCharArray();
                    for (char c = 'a'; c <= 'z'; c++) {
                        newWord[j] = c;
                        String s = new String(newWord);
                        if (!word.equals(s) && set.contains(s)) {
                            set.remove(s);
                            if (levelMap.containsKey(level)) {
                                levelMap.get(level).add(s);
                            } else {
                                List<String> newList = new ArrayList<>();
                                newList.add(s);
                                levelMap.put(level, newList);
                            }
                            if (s.equals(endWord)) {
                                levelMap.get(level).clear();
                                levelMap.get(level).add(s);
                                //no need to add more shortest we found
                                return levelMap;
                            }

                        }
                    }
                }
            }
        }

        if (!canReachEndWord)
            levelMap.clear();
        return levelMap;
    }

    private Set<String> getSmallSet(Map<Integer, List<String>> levelMap) {
        Set<String> set = new HashSet<>();
        for (int i = 1; i < levelMap.size();i++) {
            set.addAll(levelMap.get(i));
        }
        System.out.println(set.toString());
        return set;
    }
}


//from leetcode 12ms
class WL2Solution {
    // https://www.youtube.com/watch?v=vZNFOBEfib4
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> result = new ArrayList<List<String>>();

        Set<String> dict = new HashSet<>(wordList);
        if (!dict.contains(endWord))
            return result;

        Map<String, List<String>> map = new HashMap<>();
        Set<String> startSet = new HashSet<>();
        startSet.add(beginWord);
        Set<String> endSet = new HashSet<String>();
        endSet.add(endWord);
        bfs(startSet, endSet, map, dict, false);

        List<String> list = new ArrayList<String>();
        list.add(beginWord);
        dfs(result, list, beginWord, endWord, map);

        return result;

    }

    private void dfs(List<List<String>> result, List<String> list, String word, String endWord,
                     Map<String, List<String>> map) {
        if (word.equals(endWord)) {
            result.add(new ArrayList<String>(list));
            return;
        }

        if (map.get(word) == null)
            return;
        for (String next : map.get(word)) {
            list.add(next);
            dfs(result, list, next, endWord, map);
            list.remove(list.size() - 1);
        }
    }

    private void bfs(Set<String> startSet, Set<String> endSet, Map<String, List<String>> map, Set<String> dict,
                     boolean reverse) {
        // TODO Auto-generated method stub
        if (startSet.size() == 0)
            return;

        if (startSet.size() > endSet.size()) {
            bfs(endSet, startSet, map, dict, !reverse);
            return;
        }

        Set<String> tmp = new HashSet<String>();
        dict.removeAll(startSet);
        boolean finish = false;

        for (String s : startSet) {
            char[] chs = s.toCharArray();
            for (int i = 0; i < chs.length; i++) {
                char old = chs[i];
                for (char c = 'a'; c < 'z'; c++) {
                    chs[i] = c;
                    String word = new String(chs);

                    if (dict.contains(word)) {
                        if (endSet.contains(word)) {
                            finish = true;
                        } else {
                            tmp.add(word);
                        }

                        String key = reverse ? word : s;
                        String val = reverse ? s : word;

                        if (map.get(key) == null) {
                            map.put(key, new ArrayList<>());
                        }
                        map.get(key).add(val);
                    }
                }
                chs[i] = old;
            }
        }

        if (!finish) {
            bfs(tmp, endSet, map, dict, reverse);
        }
    }
}

//finally submitted to leetcode, optimization needed
class WLS3Solution{
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> resultList = new ArrayList<>();
        if (wordList.isEmpty()) {
            return resultList;
        }
        Set<String> set = new HashSet<>(wordList);
        set.remove(beginWord);
        if (!set.contains(endWord))
            return resultList;

        Map<String, List<String>> neighbourMap = buildRelationMap(beginWord, endWord, new HashSet<String>(set));
        //process
        List<String> solutionList = new ArrayList<>();
        solutionList.add(beginWord);
        findLaddersAndAdd(beginWord, endWord, neighbourMap, solutionList, resultList);

        return resultList;
    }

    private void findLaddersAndAdd(String beginWord, String endWord, Map<String, List<String>> neighbourMap, List<String> solutionList, List<List<String>> resultList) {
        if (beginWord.equals(endWord)) {
//            solutionList.add(endWord);
            resultList.add(new ArrayList<>(solutionList));
//            solutionList.remove(solutionList.size()-1);
        }
        if (neighbourMap.containsKey(beginWord)) {
            for (String word : neighbourMap.get(beginWord)) {
                solutionList.add(word);
                findLaddersAndAdd(word,endWord,neighbourMap,solutionList,resultList);
                solutionList.remove(solutionList.size()-1);
            }
        }
    }

    private Map<String, List<String>> buildRelationMap(String beginWord, String endWord, HashSet<String> set) {
        Queue<String> queue = new LinkedList<>();
        queue.add(beginWord);
        Map<String, List<String>> neighbourMap = new HashMap<>();
        Set<String> tempSet = new HashSet<>();
        String word = null;
        int levelSize = 0;
        boolean finish = false;

        while (!queue.isEmpty()) {

            System.out.println(queue.toString());
            set.removeAll(tempSet);
            tempSet.clear();
            levelSize  = queue.size();
            for (int i = 0;i<levelSize;i++) {
                word = queue.poll();

                    neighbourMap.put(word, new ArrayList<>());

                for (int j = 0; j < word.length(); j++) {
                    char[] newWord = word.toCharArray();
                    for (char c = 'a'; c <= 'z'; c++) {
                        newWord[j] = c;
                        String s = new String(newWord);
                        if (!word.equals(s) && set.contains(s)) {
                            queue.add(s);
                            tempSet.add(s);
                            neighbourMap.get(word).add(s);
                            System.out.println("parent "+word+" child = "+s);
                            if (s.equals(endWord))
                                finish = true;
                        }
                    }
                }

            }
            if (finish)
                break;
        }
        System.out.println(neighbourMap.toString());
        return neighbourMap;
    }

}
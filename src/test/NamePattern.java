package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NamePattern {
    public static void main(String[] args) {
        String[] names = {"..","anil----kumar", "anil .                              k.u.mar", "anil kumar kumar","anil- kumar"};

        for (int i = 0; i < names.length; i++)
            System.out.println("(" +names[i] + ")  -------- " + isValidName1(names[i])+"   - two alphabet = "+isTwoAlphabet(names[i]) + " replaced string = "+removeChar(names[i]));
    }

    public static boolean isValidName(String name) {
        String pattern = "([A-Za-z-]{2,25}(\\s?))*";
        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);

        // Now create matcher object.
        Matcher m = r.matcher(name);

        return m.matches();
    }

    public static boolean isValidName1(String name) {
        String pattern = "[A-Za-z-.\\s]{2,25}";
        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);

        // Now create matcher object.
        Matcher m = r.matcher(name);

        return m.matches();
    }

    public static boolean isValidMiddleName(String name) {
        String pattern = "[A-Za-z-.\\s]{2,35}";
        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);

        // Now create matcher object.
        Matcher m = r.matcher(name);

        return m.matches();
    }

    public static boolean isTwoAlphabet(String word) {
        int alphabetCount = 0;
        for (char c : word.toCharArray()) {
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <='z'))
                alphabetCount++;
            if (alphabetCount > 1)
                return true;
        }
        return false;
    }

    private static String removeChar(String word) {
        return word.replaceAll(" .-","");
    }
}

package test.kotlintest

import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

class TestKotlin {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val a = A()
            val b = A()

            println(a==b)
            println(a===b)
            println(a.equals(b))

            val book1 = Book("ds","corey")
            val book2 = Book("ds","corey")

            println(book1 == book2)
            println(book1 === book2)
        }
    }
}

class A {}

data class Book(var name: String, var authorName: String)